# Character Organizer

## Name
Character Organizer or "CharOrg" for short

## Description
The Character Organizer add-on is both a UI organizer and rigging tool for characters. I developed this add-on primarily to satisfy my own workflow with creating and using rigs that utilized many custom properties. By selecting a "Focus" object in the UI, you can more easily view custom-property controls.  
A lot of other supporting operators were implemented to assign large amounts of drivers and perform custom property creation. This process has been standardized against my existing materials and workflow, so it still remains specialized in a way that might not work for everyone.  
Nonetheless, I still wish to at least provide this if others find a use for it or wish to expand this tool to work well with their characters. 

## Installation
Please note that this add-on is working version 3.5.1 of Blender, but it may work all the way back to version 3.2. How far it's able to go back has not been explicitly tested. 
Installation of this addon is done in the same way as most other Blender addons. 
- [ ] Download the source code from this page as a .zip file.
- [ ] In Blender, open up the User Preferences window. The standard hotkey is F4, and the "Preferences" option will be at the bottom of the submenu that pops up.
- [ ] Go to the Add-ons tab on the left side. Then, select "Install..." in the top right of the menu. 
- [ ] Navigate to where the zip file was downloaded, select the file, and press the "Install Add-on" button. 
- [ ] The add-on should directly appear in your available add-on list as "CharOrg" and part of the "Rigging" category. Press the checkbox to toggle and enable the add-on. 

## Usage
The primary menu for this add-on can be found from the context panel in the 3D viewport. The default hotkey for showing (& hiding) this panel is "n". There are also additional operators in the right-click context menu when selecting color properties or custom properties from your rig. One other operator in this menu, "Retrieve Attributes", is only for debugging and does not do anything on its own. 

### Rig-Property UI
Starting with the context panel located in the viewport, a major part of this add-on is to provide an isolated but freely usable UI that displays custom properties present on certain bones on a rig object. This can be accessed through the CharOrg tab -> Main Panel -> Property Panel.  

![Property Panel - Empty](visuals/co_PPref_empty.png)

Here, you will be greeted with entry for the Focus Object. Your rig object should be selected here as that is the object the UI should pull properties from. Note that putting an non-armature object in there won't do anything, so an armature-type object is required. 

For the example, I will be selecting my character rig for my character Starlowe. The rig was made using Rigify, but this UI will work regardless of how the rig was constructed. All that is needed for this to be useful is that custom properties are present on certain pose bones. Note that properties on **Pose Bones** shown in Pose-Mode will be pulled, not properties assigned on **Edit Bones** shown in Edit Mode with the rig. Make sure that any custom properties assigned are on pose bones.  
Upon selecting the rig, the property panel will expand with more options.  
![Property Panel - Rig Initial](visuals/co_PPref_RigInit.png)

By default, custom properties will be pulled from the "Root" and "Eye controller" pose bones. The add-on searches based on given default name strings.  
Default names for the Root bone include: "root", "traj". Names will be check in order from the beginning of the list. 
Default names for the Eye-Controller bone include: "eye_common". 
Of course, not everyone uses the same naming conventions, so there are text boxes at the top of the property panel allowing for a custom bone name to be given. Technically, any bone in the rig can be pointed to by entering its name in either box. It was simply my own choice to use the Root and eye-control bones for storing the majority of my custom properties.

Beneath that is the primary part of this UI: a menu for your rig's custom properties. There are checkboxes that can toggle certain sections of the full property list that may be on the root and eye bones. If I toggle the option for the "Main Properties", the color properties I made for my character will show up.  
![Main Properties Toggle](visuals/co_PPref_MainPropsToggle.png)



## Support
For issues that are experienced using this add-on, file an issue with as many details about the problem you experienced and what exact version of Blender you are using. 

You may also reach out to me on minds.com if you have questions about the use of this add-on. My handle is @zoraxe. I will consider what other avenues for support are available in the future if the demand increases. 

## Roadmap
No explicit plans in the future. For now, the add-on is in beta-testing to see what new bugs exist and if the existing features need to be further fleshed out.
Any planned features will most likely result from either my continued use of this add-on or just working through natural issues that come up while rigging. Working through projects and discovering often repeatable tasks is what resulted in this add-on in the first place, and that's most likely how it'll continue to develop. Of course, others that use this themselves may also make suggestions for features. Such suggestions can range anywhere from new operators for rigging to more controls on existing operators specific to other people's characters types. 

## Contributing
This project is open to contributions, but I am a novice at using Gitlab. I will be learning and will update what I think are the best ways to contribute in the future. 

## Authors and acknowledgment
Primary Author & Developer: ZoraxeMB

## License
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

## Project status
This project has no explicit plans for features at the moment, but it can be modified or expanded from it's current state to satisfy other's character workflows. Specifically, a lot of the features here are meant to satisfy my own character workflows, and many of the options and property names reflect that. If you wish to suggest features or even contribute to the project, reach out to me either through here or the ways listed under the Support header. 


[//]: # (Some of the original instructions are kept for future reference on how to use Git with Gitlab in case I forget.)

# Details from original generated README for reference on how to use Gitlab

## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/Zoraxe/character-organizer.git
git branch -M main
git push -uf origin main
```

## Integrate with your tools

- [ ] [Set up project integrations](https://gitlab.com/Zoraxe/character-organizer/-/settings/integrations)

## Collaborate with your team

- [ ] [Invite team members and collaborators](https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Enable merge request approvals](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/)
- [ ] [Set auto-merge](https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)

## Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing(SAST)](https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://docs.gitlab.com/ee/user/clusters/agent/)
- [ ] [Set up protected environments](https://docs.gitlab.com/ee/ci/environments/protected_environments.html)

***

# Editing this README

When you're ready to make this README your own, just edit this file and use the handy template below (or feel free to structure it however you want - this is just a starting point!). Thank you to [makeareadme.com](https://www.makeareadme.com/) for this template.

## Suggestions for a good README
Every project is different, so consider which of these sections apply to yours. The sections used in the template are suggestions for most open source projects. Also keep in mind that while a README can be too long and detailed, too long is better than too short. If you think your README is too long, consider utilizing another form of documentation rather than cutting out information.
