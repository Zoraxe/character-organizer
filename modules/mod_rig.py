#Module for Rigging and Property Functions
import bpy
import math as m
import mathutils as mu
import numpy as np
from rna_prop_ui import rna_idprop_ui_create as prop_create
from .ref_props import *    #Various standard properties

#Driver functions
def drv_add(prop_loc, prop, var_obj, var_loc, var_name=[df_name], expression=df_exp, *, ind=None, id_type='OBJECT'):  #function to add a driver
    if ind == None:     #no index involved
        drv = prop_loc.driver_add(prop).driver  #driver object reference
    else:   #index used to mark property
        drv = prop_loc.driver_add(prop, ind).driver  #driver object reference
    var_amount = len(var_obj)   #number of variables to create
    vars = [None]*var_amount    #list to hold all variable objects
    for i in range(len(var_obj)):   #create a variable for every object reference
        vars[i] = drv.variables.new()   #creates a new variable within the driver
        vars[i].name = var_name[i]  #set the varible name
        vars[i].targets[0].id_type = id_type    #set the type of object to reference from
        vars[i].targets[0].id = var_obj[i]  #set the variable's object reference
        vars[i].targets[0].data_path = var_loc[i]   #set the reference location within the selected object
    drv.expression = expression     #set the driver expression
    return [drv, vars]
#quick pose bone string function
def pb_string(obj_name, prop_name):
    prop_ref = 'pose.bones["' + obj_name + '"]["' + prop_name + '"]'
    return prop_ref
#Function for creating expressions or extensions for drivers
def exp_ext(var_name, inverse=False, extension=True):
    expression = ""
    if inverse:     #Use the inverse expression type if true
        if extension:   #for extending expressions
            expression = "or(" + var_name + ")"
        else:   #no extension prefix
            expression = "(" + var_name + ")"
    else:   #use the normal expression type
        if extension:   #for extension expression
            expression = "*(1-" + var_name + ")"
        else:   #not extension prefix
            expression = "(1-" + var_name + ")"
    return expression

#Function to check for valid root bone
def root_check(focus_obj, custom_input = "", defaults = root_name):
    try:
        if custom_input != "":  #if the string isn't empty, check to see if bone exists
            try:
                root_bone = focus_obj.pose.bones[custom_input]  #root-bone reference, use input
            
            except:     #bone from custom input was not found
                for name in defaults:  #cycle through all possible bone names in the root_name list
                    try:    #attempt to find a bone from the provided name list
                        root_bone = focus_obj.pose.bones[name]
                    except: 
                        pass
        else:
            for name in defaults:  #cycle through all possible bone names in the root_name list
                try:    #attempt to find a bone from the provided name list
                    root_bone = focus_obj.pose.bones[name]
                except: 
                    pass
        #print("Root bone found on " + focus_obj.name, " as " + root_bone.name)     #DEBUG attempt the print statement to determine if root bone was actually found or not
        return root_bone
    except:
        #print("No root bone found on " + focus_obj.name + ".")
        return None

######################
#Constraint Functions
######################

#transform constraint, rotation into location adjustment
def rot_loc(rig_obj, name, cst_bone, ctrl_bone, *, 
    loc_dir = 'Z', rot_dir = 'X', rot = m.pi/12, loc = -0.075, ext = False
):
    cst = cst_bone.constraints.new('TRANSFORM')
    cst.name = name     #set the name of the constraint
    cst.target = rig_obj    #target is the rig object
    cst.subtarget = ctrl_bone.name  #subtarget reference ctrl bone name
    cst.use_motion_extrapolate = ext    #set the extrapolate option
    cst.target_space = 'LOCAL'  
    cst.owner_space = 'LOCAL'   #Set both the target and owner space to local
    cst.map_from = 'ROTATION'   #map from the ctrl's rotation
    if rot_dir == 'X':  #X-rotation reference
        cst.from_max_x_rot = rot    #set the rotation range
    elif rot_dir == 'Y':    #Y-rotation reference
        cst.from_max_y_rot = rot    #set the rotation range
    elif rot_dir == 'Z':    #Z-rotation reference
        cst.from_max_z_rot = rot    #set the rotation range
    if loc_dir == 'X':  #location axis to transform along
        cst.map_to_x_from = rot_dir     #use the rotation axis to influence location movement
        cst.to_max_x = loc    #set the range of movement
    elif loc_dir == 'Y':
        cst.map_to_y_from = rot_dir     #use the rotation axis to influence location movement
        cst.to_max_y = loc    #set the range of movement
    elif loc_dir == 'Z':
        cst.map_to_z_from = rot_dir     #use the rotation axis to influence location movement
        cst.to_max_z = loc    #set the range of movement
    return cst
#transform constraint, location into location adjustment
def loc_loc(rig_obj, name, cst_bone, ctrl_bone, *, 
    loc1_dir = 'Z', loc2_dir = 'Y', loc1 = 0.1, loc2 = 0.1, ext = False
):
    cst = cst_bone.constraints.new('TRANSFORM')
    cst.name = name     #set the name of the constraint
    cst.target = rig_obj    #target is the rig object
    cst.subtarget = ctrl_bone.name  #subtarget reference ctrl bone name
    cst.use_motion_extrapolate = ext    #set the extrapolate option
    cst.target_space = 'LOCAL'  
    cst.owner_space = 'LOCAL'   #Set both the target and owner space to local
    if loc1_dir == 'X':  #X-rotation reference
        cst.from_max_x = loc1    #set the rotation range
    elif loc1_dir == 'Y':    #Y-rotation reference
        cst.from_max_y = loc1    #set the rotation range
    elif loc1_dir == 'Z':    #Z-rotation reference
        cst.from_max_z = loc1    #set the rotation range
    if loc2_dir == 'X':  #location axis to transform along
        cst.map_to_x_from = loc1_dir     #use the rotation axis to influence location movement
        cst.to_max_x = loc2    #set the range of movement
    elif loc2_dir == 'Y':
        cst.map_to_y_from = loc1_dir     #use the rotation axis to influence location movement
        cst.to_max_y = loc2    #set the range of movement
    elif loc2_dir == 'Z':
        cst.map_to_z_from = loc1_dir     #use the rotation axis to influence location movement
        cst.to_max_z = loc2    #set the range of movement
    return cst
#Copy Rotation constraint, custom influence
def rot_const(rig_obj, name, cst_bone, ctrl_bone, inf, *, 
    mix_mode = 'ADD', target_space = 'LOCAL', owner_space = 'LOCAL'
):
    cst = cst_bone.constraints.new('COPY_ROTATION')     #Create the constraint
    cst.name = name     #set the constraint name
    cst.target = rig_obj    #target is the rig object
    cst.subtarget = ctrl_bone.name  #subtarget reference ctrl bone name
    cst.mix_mode = mix_mode    #set how the constraint transform will be applied to the existing transforms
    cst.target_space = target_space     #set the space to reference on the target
    cst.owner_space = owner_space   #set the space to apply transforms to on the constraint owner
    cst.influence = inf     #set the influence on the constraint
    return cst