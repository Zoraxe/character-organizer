#Function: convert numbers 0 to 25 to corresponding alphabetical letter. Standard letter to start from is capital 'A'
def ntl(num, ref_char = 'A'):
    if num < 0 or num > 25:
        return "Invalid Input"
    return chr(ord(ref_char) + num)

#Standard property names and strings
tnode_name = "Toon_Main"    #standard toon-node name
root_name = ("root", "Root", "traj", )  #Common root-bone names, descending priority
eyeCtrl_name = ("eye_common", )     #Common eye-control name
jaw_name = ("jaw_master", )     #jaw control name
fthrL_name = ("fthrOuter.L", )    #feather control names, left side
fthrR_name = ("fthrOuter.R", )    #feather control names, right side    

#property names
toon_prop_names = ["Tn_PBR-NPR", "Tn_Saturation", "Tn_BaseValue", "Tn_ValueHL", "Tn_ValueS", 
    "TnS_Color", "Tn_Solid", 
    "Tn_RangeHL", "Tn_RangeS", 
]

to_standard = "ToonOutline"     #Standard toon outline name
to_scale = "TnO_Scale"  #Standard toon outline scale name
#property dictionaries
prop_dict = {
    #Color Properties
    "BaseColor": "Base Color", 
    "MainColor": "Main Color", 
    "PrimaryColor": "Primary Color", 
    "SecondaryColor": "Secondary Color", 
    "WhitesColor": "Whites Color", 
    "DarksColor": "Darks Color", 
    "NailsColor": "Nails Color", 
    "MouthColor": "Mouth Color", 
    "TeethColor": "Teeth Color", 
    "TongueColor1": "Tongue Main Color", 
    "TongueColor2": "Tongue Secondary Color", 
    "Beak_MainColor": "Beak Main Color", 
    "Beak_SecondaryColor": "Beak Secondary Color", 
    "Beak_TertiaryColor": "Beak Tertiary Color", 
    "Value_Overall": "Value Master", 
    "Hue_Overall": "Hue Master", 
    "Saturation_Overall": "Saturation Master", 
    #Toon Properties
    "ToonOutline": "Toon Outlines", 
    "TnS_Color": "Toon Solid Color", 
    "TnO_Color": "Toon Outline Color", 
    "Tn_PBR-NPR": "Shading Type", 
    "Tn_Saturation": "Saturation", 
    "Tn_BaseValue": "Base Value", 
    "Tn_ValueHL": "Highlight Value", 
    "Tn_ValueS": "Shadow Value", 
    "Tn_Solid": "Toon Solid Option", 
    "Tn_RangeHL": "Highlight Range", 
    "Tn_RangeS": "Shadow Range", 
    "Tn_Value_Overall": "Toon Value Master", 
    "TnO_Scale": "Toon Outline Scale", 
    #Eye Properties
    "parent_switch": "Parent Switch", 
    "Eye_Brightness": "Brightness", 
    "Eye_Reflection": "Reflection Factor", 
    "Eye_Hue": "Hue",
    "Eye_Saturation": "Saturation",  
    #Mech-Rig Specifics
    "manual_master": "Manual - Master", 
    "hide_master": "Hide - Master", 
}
#driver props
df_name = "var"     #default name
df_exp = "var"  #default expression
drv_stan = ("mn", "h")  #standard variable prefixes

#Standard custom properties for rigs
#Snaryan property extension
snaryan_prop_ext = [     #property extensions specific to Snaryan characters
    ["Snr_BaseColor", "Snr_SecondaryColor", "Snr_WhitesColor", "Snr_DarksColor", "Snr_NailsColor", "Snr_MouthColor", 
    "Snr_TeethColor", "Snr_TongueColor1", "Snr_TongueColor2"],     #prop names 
    [   #prop_defaults  (Substitute with user inputs if possible)
        [0.002400, 0.004512, 0.012000, 1.000000], 
        [0.035600, 0.050000, 0.005000, 1.000000], 
        [0.070000, 0.067200, 0.042000, 1.000000], 
        [0.001650, 0.002000, 0.001300, 1.000000], 
        [0.050000, 0.037000, 0.017500, 1.000000], 
        [0.100000, 0.022690, 0.022690, 1.000000],
        [0.800000, 0.654400, 0.520000, 1.000000], 
        [0.270000, 0.013500, 0.013500, 1.000000], 
        [0.000270, 0.000014, 0.000014, 1.000000] 
    ],     
    [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0],     #prop min
    [1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0],     #prop max
    [   #prop descriptions
      "Snaryan Colors: Set the base color", 
      "Snaryan Colors: Set the Secondary Color", 
      "Snaryan Colors: Set the Whites Color", 
      "Snaryan Colors: Set the Darks Color", 
      "Snaryan Colors: Set the Nails Color", 
      "Snaryan Colors: Set the Mouth Color",   
      "Snaryan Colors: Set the Teeth Color", 
      "Snaryan Colors: Set the main Tongue Color", 
      "Snaryan Colors: Set the secondary Tongue Color"
    ],   
    ['COLOR', 'COLOR', 'COLOR', 'COLOR', 'COLOR', 'COLOR', 'COLOR', 'COLOR', 'COLOR'],     #prop subtypes  
]
#Cowler property extension
cowler_prop_ext = [     #property extensions specific to Snaryan characters
    ["Cwl_PrimaryColor", "Cwl_SecondaryColor", "Cwl_DarksColor", "Cwl_NailsColor", "Cwl_MouthColor", 
    "Cwl_Beak_MainColor", "Cwl_Beak_SecondaryColor", "Cwl_Beak_TertiaryColor", 
    "Cwl_TongueColor1", "Cwl_TongueColor2"],     #prop names 
    [   #prop_defaults  (Substitute with user inputs if possible)
        [0.009882, 0.009500, 0.010000, 1.000000],   #Primary Color
        [0.049500, 0.049663, 0.050000, 1.000000],   #Secondary Color
        [0.015000, 0.012562, 0.005250, 1.000000],   #Darks Color
        [0.500000, 0.456225, 0.325000, 1.000000],   #Nails Color
        [0.100482, 0.025843, 0.025843, 1.000000],   #Mouth Color
        [0.000976, 0.000900, 0.001000, 1.000000],   #Beak Main Color
        [0.024750, 0.024831, 0.025000, 1.000000],   #Beak Secondary Color
        [0.190000, 0.193260, 0.200000, 1.000000],   #Beak Tertiary Color
        [0.270000, 0.013500, 0.013500, 1.000000],   #Tongue Color 1
        [0.001000, 0.001000, 0.001000, 1.000000]    #Tongue Color 2
    ],     
    [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0],     #prop min
    [1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 0.0],     #prop max
    [   #prop descriptions
      "Cowler Colors: Set the Primary color", 
      "Cowler Colors: Set the Secondary Color", 
      "Cowler Colors: Set the Darks Color", 
      "Cowler Colors: Set the Nails Color", 
      "Cowler Colors: Set the Mouth Color",   
      "Cowler Colors: Set the Beak Main Color", 
      "Cowler Colors: Set the Beak Secondary Color", 
      "Cowler Colors: Set the Beak Tertiary Color", 
      "Cowler Colors: Set the main Tongue Color", 
      "Cowler Colors: Set the secondary Tongue Color"
    ],   
    ['COLOR', 'COLOR', 'COLOR', 'COLOR', 'COLOR', 'COLOR', 'COLOR', 'COLOR', 'COLOR', 'COLOR'],     #prop subtypes  
]
#Default black and white values
def_color_bl, def_color_wh = (0.0, 0.0, 0.0, 1.0), (1.0, 1.0, 1.0, 1.0)
#Standard Root properties
root_prop_ext = [
    #Property names
    ["Value_Overall", "Hue_Overall", "Saturation_Overall", 
    to_standard, "TnO_Color", to_scale, 
    *toon_prop_names
    ], 
    #List of property defaults, corresponding to names list
    [1.0, 0.5, 1.0, 
    0, def_color_bl, 1.0, 
    1.0, 1.0, 1.0, 3.0, 0.25, 
    def_color_wh, 0,
    1.0, -2.0
    ], 
    #List of mins and maxes
    [0.0, -1.0, 0.0, 
    0, 0.0, 0.0, 
    0.0, 0.0, 0.0, 0.0, 0.0,  
    0.0, 0, 
    -50.0, -50.0
    ], 
    [10.0, 2, 10.0, 
    1, 1.0, 20.0, 
    1.0, 10.0, 10.0, 30.0, 10.0, 
    1.0, 1, 
    50.0, 50.0
    ], 
    #List of property descriptions
    ["Control the overall value of all materials part of the rig", 
    "Control the overall hue of materials part of the rig", 
    "Control the overall saturation of materials part of the rig", 
    "Toggle the use of the Toon Outlines. Recommended to be using the Eevee Render Engine when enabled", 
    "Toon Shading Outline: Set the outline color", 
    "Toon Shading Outline: Scale the outline width across all rig objects", 
    "Toon Shading: Switch between PBR and NPR Shading", 
    "Toon Shading: Set the overall saturation", 
    "Toon Shading: Set the value for the base colors", 
    "Toon Shading: Set the value for the highlight colors", 
    "Toon Shading: Set the value for the shadow colors", 
    "Toon Shading Solid: Set the solid color if it is enabled", 
    "Toon Shading: Toggle the use of solid colors", 
    "Toon Shading: Set the highlight range value at which the highlight colors begin to show", 
    "Toon Shading: Set the shadow range value at which the shadow colors begin to show", 
    ], 
    #List of property subtypes
    ['NONE', 'NONE', 'NONE', 
    'NONE', 'COLOR', 'NONE', 
    'NONE', 'NONE', 'NONE', 'NONE', 'NONE', 
    'COLOR', 'NONE', 
    'NONE', 'NONE', 
    ]
]
#Standard eye properties
eye_prop_ext = [
    #property names
    ["Eye_Brightness", "Eye_Reflection", "Eye_Hue", "Eye_Saturation"],   
    #defaults 
    [1.0, 1.0, 0.5, 1.0],     
    [0.0, 0.0, -1.0, 0.0],     #min
    [10.0, 1.0, 2.0, 10.0],    #max
    [   #descriptions
        "Set the brightness values of the eyes. This will be multiplied against the chosen Overall Value", 
        "Set the Reflectiveness or glossiness of the eyes", 
        "Set the hue for the eyes' color", 
        "Set the saturation for the eyes' color"
    ],
    #Property subtypes 
    ['NONE', 'NONE', 'NONE', 'NONE']
]
#Deformation bone groups
#Snaryan deform groups
snr_def_teeth = ("DEF-teeth_top", "DEF-teeth.B")    #Teeth deformation bone names
snr_def_tongue = (    #tongue deformation bones
    "DEF-tongue", 
    "DEF-tongue.001", 
    "DEF-tongue.002", 
    "DEF-tongue.003", 
    "DEF-tongue.004", 
    "DEF-tongue.005", 
    "DEF-tongue_fork.L", "DEF-tongue_fork.R", 
    "DEF-tongue_fork.L.001", "DEF-tongue_fork.R.001", 
)
snr_def_eye = (   #eye deformation bones
    "DEF-eye_master.L", 
    "DEF-eye_master.R", 
    "DEF-eye.L", 
    "DEF-eye.R", 
    "DEF-eye_iris.L", 
    "DEF-eye_iris.R"
)
snr_def_eyelid = (  #eyelid deformation bones
    "DEF-eyelid_T1.L", "DEF-eyelid_T1.L.001", "DEF-eyelid_T1.L.002", "DEF-eyelid_T1.L.003", "DEF-eyelid_T1.L.004", 
    "DEF-eyelid_T2.L", "DEF-eyelid_T2.L.001", "DEF-eyelid_T2.L.002", "DEF-eyelid_T2.L.003", "DEF-eyelid_T2.L.004", 
    "DEF-eyelid_rear.L", "DEF-eyelid_rear.L.001", "DEF-eyelid_rear.L.002", "DEF-eyelid_rear.L.003", 
    "DEF-eyelid_T1.R", "DEF-eyelid_T1.R.001", "DEF-eyelid_T1.R.002", "DEF-eyelid_T1.R.003", "DEF-eyelid_T1.R.004", 
    "DEF-eyelid_T2.R", "DEF-eyelid_T2.R.001", "DEF-eyelid_T2.R.002", "DEF-eyelid_T2.R.003", "DEF-eyelid_T2.R.004", 
    "DEF-eyelid_rear.R", "DEF-eyelid_rear.R.001", "DEF-eyelid_rear.R.002", "DEF-eyelid_rear.R.003", 
    "DEF-eyelid_B1.L", "DEF-eyelid_B1.L.001", "DEF-eyelid_B1.L.002", "DEF-eyelid_B1.L.003", "DEF-eyelid_B1.L.004", 
    "DEF-eyelid_B2.L", "DEF-eyelid_B2.L.001", "DEF-eyelid_B2.L.002", "DEF-eyelid_B2.L.003", "DEF-eyelid_B2.L.004", 
    "DEF-eyelid_rearB.L", "DEF-eyelid_rearB.L.001", "DEF-eyelid_rearB.L.002", "DEF-eyelid_rearB.L.003", 
    "DEF-eyelid_B1.R", "DEF-eyelid_B1.R.001", "DEF-eyelid_B1.R.002", "DEF-eyelid_B1.R.003", "DEF-eyelid_B1.R.004", 
    "DEF-eyelid_B2.R", "DEF-eyelid_B2.R.001", "DEF-eyelid_B2.R.002", "DEF-eyelid_B2.R.003", "DEF-eyelid_B2.R.004", 
    "DEF-eyelid_rearB.R", "DEF-eyelid_rearB.R.001", "DEF-eyelid_rearB.R.002", "DEF-eyelid_rearB.R.003", 
)
snr_def_backfin = (     #back-fin deformation bones
    "DEF-Fin_Back.000", 
    "DEF-Fin_Back.001", 
    "DEF-Fin_Back.002"
)
#General deform groups
def_hat = ("DEF-Hat", "DEF-hat")     #hat deform bone

#Color Properties
snr_colors = ("Snr_BaseColor", "Snr_SecondaryColor", "Snr_WhitesColor", "Snr_DarksColor", "Snr_NailsColor", "Snr_MouthColor")   #Snaryan list of color property names
snode_props = (
    snr_colors,     #Snaryan Main Shader
    snr_colors,     #Snaryan Main Shader
    ("Snr_TeethColor", ),   #Teeth Shader
    ("Snr_TongueColor1", "Snr_TongueColor2")    #Tongue Shader
)   
snode_ind = (     #indices in each node to add drivers to
    [2, 10, 18, 26, 34, 37],    #Snaryan Main Shader
    [0, 2, 6, 10, 13, 16],      #Snaryan Toon Colors
    (0, ),    #Teeth Shader
    (0, 1)  #Tongue Shader
)
snode_names = ("Snaryan_Main", "Snaryan_Toon", "Teeth_Main", "Tongue_Main")     #standard node names 


####################
# Property Functions
####################
def focus_retrieve(ctx_scene, focus_opt = '1'):
    try:
        if focus_opt == '1':
            focus = ctx_scene.obj_focus1
            root = ctx_scene.obj_focus1_root
            eye = ctx_scene.obj_focus1_eye
        elif focus_opt == '2':
            focus = ctx_scene.obj_focus2
            root = ctx_scene.obj_focus2_root
            eye = ctx_scene.obj_focus2_eye
        elif focus_opt == '3':
            focus = ctx_scene.obj_focus3
            root = ctx_scene.obj_focus3_root
            eye = ctx_scene.obj_focus3_eye
        elif focus_opt == '4':
            focus = ctx_scene.obj_focus4
            root = ctx_scene.obj_focus4_root
            eye = ctx_scene.obj_focus4_eye
        elif focus_opt == '5':
            focus = ctx_scene.obj_focus5
            root = ctx_scene.obj_focus5_root
            eye = ctx_scene.obj_focus5_eye
        return{
            "focus": focus, 
            "root": root, 
            "eye": eye, 
        }
    except:
        return{
            "focus": None, 
            "root": None, 
            "eye": None, 
        }
