#Define the UI panel classes
import bpy
import textwrap     #text-wrapping module from Python
from .modules.ref_props import *    #Import all reference properties


wrap_mod = textwrap.TextWrapper(width=40)  #sets the maximum length of text before it gets wrapped
def wrap_text(layout_item, text):    #function to word wrap for labels
    wrapped_text = wrap_mod.wrap(text = text)
    for txt in wrapped_text:
        row = layout_item.row(align = True)
        row.alignment = 'EXPAND'
        row.scale_y = 0.5
        row.label(text = txt)
    return
Panel = bpy.types.Panel     #Panel class Mix-in
class CorgPanel:    #Base Class Panel Mix-in
    bl_space_type = "VIEW_3D"
    bl_region_type = "UI"
    bl_category = "CharOrg"

#Common name parameters
cl_pref_rig = "CRIG_PT_"    #for char-rig classes
main_panel = cl_pref_rig + "MainPanel"  #panel id
prop_panel = cl_pref_rig + "PropPanel"    

#Common texts & labels
add_text1 = "Add objects to existing rig"
#add_text2 = "*Active object must a mech-rig type"
rm_text1 = "Remove objects from rig"
#rm_text2 = "*Selected objects must be from same rig"
stan_prfx = ("Tn", "Snr_", "Clth_", "Cwl_", "Vlp_")     #tuple of standard prefixes
excl_params = ("rigify_parameters", )   #parameters to exclude from UI if encountered

#Main panel
class CORG_PT_MainPanel(Panel, CorgPanel):
    bl_idname = main_panel
    bl_label = "Main Panel"
    
    def draw(self, context):
        layout = self.layout
        scn = context.scene

#Function for appending display properties to an existing column object
def panel_appendCol(col, prop_obj, prefix, item_list, prfx_trunc = False):
    if type(prefix) != tuple and type(prefix) != list:   #Not iterable, use inclusion process for prefix
        for item in item_list:
            if not item in excl_params:     #exclude certain parameters by name
                if prefix in item:  #check if prefix substring is in the item
                    if prfx_trunc:  #truncate the item string to exclude prefix
                        try:    #try to see if item is in property dictionary
                            item_label = prop_dict[item[len(prefix):len(item)]]
                        except:     #item not in dictionary. Simply use item name as is
                            item_label = item[len(prefix):len(item)]
                    else:   #no truncation of item name
                        try:    #try to see if item is in property dictionary
                            item_label = prop_dict[item]
                        except:     #item not in dictionary. Simply use item name as is
                            item_label = item
                    #Create the panel property from the given panel object at the property path
                    col.prop(prop_obj, '["' + item + '"]', text = item_label)
    else:   #Iterable, use exclusion process for given set of prefixes or substrings
        for item in item_list:
            if not item in excl_params:     #exclude certain parameters by name
                prfx_check = True   #boolean for checking substrings
                for prfx in prefix:     #loop through all prefixes in given list
                    if prfx in item:    #substring exists in item: exclude item from panel group
                        prfx_check = False
                        break
                if prfx_check:  #item does not have substrings and will be included in panel group
                    if prfx_trunc:  #truncate the item string to exclude prefix
                        try:    #try to see if item is in property dictionary
                            item_label = prop_dict[item[len(prefix):len(item)]]
                        except:     #item not in dictionary. Simply use item name as is
                            item_label = item[len(prefix):len(item)]
                    else:   #no truncation of item name
                        try:    #try to see if item is in property dictionary
                            item_label = prop_dict[item]
                        except:     #item not in dictionary. Simply use item name as is
                            item_label = item
                    #Create the panel property from the given panel object at the property path
                    col.prop(prop_obj, '["' + item + '"]', text = item_label)
    return col

#Panel group function based on prefix (or exclusion of prefixes)
def panel_group(pnl_obj, prop_obj, prefix, item_list, prfx_trunc = False, grp_title = ""):
    col = pnl_obj.column()
    if grp_title != "":     #Give a column title if one is given
        #Only show the title header if there is a single property that can be displayed from the list
        if type(prefix) != tuple and type(prefix) != list:   #Not iterable, use inclusion process for prefix
            for item in item_list:
                if prefix in item:  #check if prefix substring is in the item
                    col.label(text = grp_title)
                    break
        else:   #Iterable, use exclusion process for given set of prefixes or substrings
            for item in item_list:
                prfx_check = True   #boolean for checking substrings
                for prfx in prefix:     #loop through all prefixes in given list
                    if prfx in item:    #substring exists in item: exclude item from panel group
                        prfx_check = False
                        break
                if prfx_check:  #item does not have substrings and will be included in panel group
                    col.label(text = grp_title)
                    break
    return panel_appendCol(col, prop_obj, prefix, item_list, prfx_trunc)    #append the properties to the panel group/column

#Function to attempt to pull properties of a specific object/bone
def panel_getProps(col, rig_obj, prop_bone_strings, prefix, prfx_trunc = False):
    #Take the prop_object string and try to retrieve a pose bone
    for prop_bone_string in prop_bone_strings:  #cycle through the options in the given list/tuple until a property is found
        try:    #attempt to retrieve the pose bone objects and its keys
            prop_bone = rig_obj.pose.bones[prop_bone_string]     #retrieve the pose bone
            prop_keys = prop_bone.keys()    #retrieve the property keys
            panel_appendCol(col, prop_bone, prefix, prop_keys, prfx_trunc)  #append the properties to the UI column
        except: 
            continue    #go to next entry if nothing is found from this one
    return
#panel to display properties from chosen objects
class CORG_PT_PropPanel(Panel, CorgPanel):
    bl_idname = prop_panel
    bl_label = "Property Panel"
    bl_parent_id = main_panel
    bl_options = {'DEFAULT_CLOSED'}

    def draw(self, context):
        layout = self.layout
        scn = context.scene
        #Create a dropdown menu for available objects
        layout.prop(scn, "focus_set", expand = True)
        box = layout.box()
        col = box.column()
        col.label(text = "Focus Object")
        col.prop(scn, "obj_focus" + scn.focus_set, text = "")
        focus_obj = focus_retrieve(scn, scn.focus_set)  #dynamically retrieve the focus object based on the chosen focus_set

        obj_foci = (focus_obj["focus"], )   #tuple of focus objects
        for focus in obj_foci:  #loop through all objects in list
            if focus != None:  #make sure an object is present in the pointer property
                if focus.type == 'ARMATURE':   #Check for armature/rig objects
                    col.label(text = "Custom Root:")
                    col.prop(scn, "obj_focus" + scn.focus_set + "_root", text = "")
                    col.label(text = "Custom Eye CTRL:")
                    col.prop(scn, "obj_focus" + scn.focus_set + "_eye", text = "")
                    col.operator('view3d.reset_default_properties')
                    try:    #Root bone properties
                        if focus_obj["root"] != "":   #if the string isn't empty, check to see if bone exists
                            try:
                                root_bone = focus.pose.bones[focus_obj["root"]]   #root-bone reference, user input
                            except:
                                for i in range(len(root_name)):
                                    try:    #loop through the series of names to attempt to find a bone
                                        root_bone = focus.pose.bones[root_name[i]]     #root-bone reference, standard
                                    except: 
                                        pass
                        else:
                            for i in range(len(root_name)):
                                try:    #loop through the series of names to attempt to find a bone
                                    root_bone = focus.pose.bones[root_name[i]]     #root-bone reference
                                except: 
                                    pass
                        root_items = root_bone.keys()   #List of custom props on the bone
                        box2 = box.box()
                        #Toon Properties
                        box2.prop(scn, "toon_toggle")
                        if scn.toon_toggle:
                            try: #Show the Toon Outline property among the other toon property controls
                                box2.prop(root_bone, '["' + to_standard + '"]', text = prop_dict[to_standard])
                            except:
                                pass
                            panel_group(box2, root_bone, stan_prfx[0], root_items, False, "")
                        #Snaryan Properties
                        box2.prop(scn, "main_toggle")
                        if scn.main_toggle:
                            panel_group(box2, root_bone, stan_prfx[1], root_items, True, "Snaryan Properties:")
                        #Cowler Properties
                        if scn.main_toggle:
                            panel_group(box2, root_bone, stan_prfx[3], root_items, True, "Cowler Properties:")
                        #Velpryn Properties
                        if scn.main_toggle:
                            panel_group(box2, root_bone, stan_prfx[4], root_items, True, "Velpryn Properties:")
                        #Clothing Properties
                        box2.prop(scn, "clothing_toggle")
                        if scn.clothing_toggle:
                            panel_group(box2, root_bone, stan_prfx[2], root_items, True, "")
                        #Miscellaneous
                        box2.prop(scn, "other_toggle")
                        if scn.other_toggle:
                            misc_col = panel_group(box2, root_bone, stan_prfx, root_items, False, "Other Properties:")
                            #Other bones to pull properties from
                            panel_getProps(misc_col, focus, jaw_name, stan_prfx)
                            panel_getProps(misc_col, focus, fthrL_name, stan_prfx)
                            panel_getProps(misc_col, focus, fthrR_name, stan_prfx)
                        #Selected Bone Properties
                        box2.prop(scn, "sel_bone_toggle")
                        if scn.sel_bone_toggle: 
                            sel_col = box2.column()
                            if context.active_pose_bone != None:    #check if a pose bone is actually selected or not
                                panel_getProps(sel_col, focus, (context.active_pose_bone.name, ), stan_prfx)
                    except:     #No valid root bone found
                        #print("No root bone found on " + focus.name + ". Skipping...")
                        pass
                        
                    try:    #Eye controller properties
                        if focus_obj["eye"] != "":    #if the string isn't empty, check to see if bone exists
                            try:
                                eye_bone = focus.pose.bones[focus_obj["eye"]]     #eye-control reference, user input
                            except:
                                for i in range(len(eyeCtrl_name)):
                                    try: #loop through the series of names to attempt to find a bone
                                        eye_bone = focus.pose.bones[eyeCtrl_name[i]]   #eye-control reference, standard
                                    except: 
                                        pass
                        else:
                            for i in range(len(eyeCtrl_name)):
                                try: #loop through the series of names to attempt to find a bone
                                    eye_bone = focus.pose.bones[eyeCtrl_name[i]]   #eye-control reference, standard
                                except: 
                                    pass
                        eye_items = eye_bone.keys()     #List of custom props on the bone
                        box2 = box.box()
                        col = box2.column()
                        #col.label(text = "Eye Properties:")
                        col.prop(scn, "eye_toggle")
                        if scn.eye_toggle:
                            for item in eye_items:
                                if not item in excl_params:     #exclude certain parameters by name
                                    try:    #try to see if item is in property dictionary
                                        item_label = prop_dict[item]
                                    except:     #item not in dictionary. Simply use item name as is
                                        item_label = item
                                    col.prop(eye_bone, '["' + item + '"]', text = item_label)
                    except:
                        #print("No eye controller bone found on ")
                        pass
                        
            #     else:   #Focus object is not an armature-type
            #         scn.root_valid = False
            # else: #No focus object defined
            #     scn.root_valid = False
        layout.prop(scn, "focus_set", expand = True)
                
                    
#panel for properties that apply to specific character-types
class CORG_PT_CharacterPanel(Panel, CorgPanel):
    bl_idname = cl_pref_rig + "CharacterPanel"
    bl_label = "Character Controls"
    bl_parent_id = main_panel
    bl_options = {'DEFAULT_CLOSED'}

    def draw(self, context):
        layout = self.layout
        scn = context.scene
        #Simple Driver Operators
        col = layout.column()
        col.label(text = "Quick Drivers:")
        col.operator('view3d.char_driver_colors', icon='COLOR')
        col.operator('view3d.char_driver_toon')

#panel for property creation on selected character rigs
class CORG_PT_PropertyCreationPanel(Panel, CorgPanel):
    bl_idname = cl_pref_rig + "PropertyCreationPanel"
    bl_label = "Property Creation"
    bl_parent_id = cl_pref_rig + "CharacterPanel"
    bl_options = {'DEFAULT_CLOSED'}

    def draw(self, context):
        layout = self.layout
        scn = context.scene
        #Color Property operator
        #layout.split()
        box = layout.box()
        box.label(text = "Create color property for Root bone")
        col = box.column()
        col.label(text = "Property Name")
        col.prop(scn, "root_color_name", text = "")
        col.prop(scn, "root_color_default", text = "Default Color")
        col.label(text = "Property Description")
        col.prop(scn, "root_color_desc", text = "")
        col.operator('view3d.create_color_property')
        box = layout.box()
        box.label(text = "Quick Properties:")
        col = box.column()
        col.operator('view3d.add_standard_properties')

#panel for toon controls & operators
class CORG_PT_ToonControlPanel(Panel, CorgPanel):
    bl_idname = cl_pref_rig + "ToonControlPanel"
    bl_label = "Toon Controls"
    bl_parent_id = cl_pref_rig + "CharacterPanel"
    bl_options = {'DEFAULT_CLOSED'}

    def draw(self, context):
        layout = self.layout
        scn = context.scene
        #Toon-Outline Operator
        box = layout.box()
        box.label(text = "Add Toon Outline Controls")
        col = box.column()
        col.label(text = "Toon Outline Material:")
        col.prop(scn, "toon_ol_mat", text = "")
        col.label(text = "Toon Solidify Thickness:")
        col.prop(scn, "toon_solidify_thickness", text = "")
        col.prop(scn, "toon_solidify_override_th")
        col.prop(scn, "toon_solidify_defaults", text = "Use Defaults")
        if not scn.toon_solidify_defaults:
            col.label(text = "Toon Solidify Offset:")
            col.prop(scn, "toon_solidify_offset", text = "")
            col.prop(scn, "toon_solidify_override_os")
            col.label(text = "Solidify Modifier Name:")
            col.prop(scn, "toon_solidify_name", text = "")
            col.prop(scn, "use_weighted_normal")
            if scn.use_weighted_normal:
                col.label(text = "Weighted-Normal Modifier Name:")
                col.prop(scn, "toon_wn_name", text = "")
        col.operator('view3d.toon_solidify_add')

#panel for custom Rigify generation
class CORG_PT_RigGeneration(Panel, CorgPanel):
    bl_idname = cl_pref_rig + "RigGenPanel"
    bl_label = "Rig Generation"
    bl_parent_id = cl_pref_rig + "CharacterPanel"
    bl_options = {'DEFAULT_CLOSED'}

    def draw(self, context):
        layout = self.layout
        scn = context.scene
        #Rigify Generation Operator and Settings
        box = layout.box()
        box.label(text = "Rigify Custom Generation")
        col = box.column()
        col.label(text = "Metarig Target:")
        col.prop(scn, "focus_metarig", text = "")
        col = box.column()
        col.scale_y = 1.5
        col.label(text = "*Metarig target needs to match currently focused rig before generating")
        col.operator('view3d.rigify_generate_extra')
        #Constraint controls
        box_constraints = box.box()
        col = box_constraints.column()
        col.label(text = "Constraint Controls:")
        col.prop(scn, "eye_constraints")
        if scn.eye_constraints == 'SNARYAN':
            col.prop(scn, "snr_eyelid_loc1")
            col.prop(scn, "snr_eyelid_loc2")
        elif scn.eye_constraints == 'COWLER':
            col.prop(scn, "cwl_eyelid_loc1")
            col.prop(scn, "cwl_eyelid_loc2")
        col.prop(scn, "jaw_constraints")
        if scn.jaw_constraints == 'SNARYAN':
            col.prop(scn, "snr_mouth_rot")
            col.prop(scn, "snr_mouth_loc")
        elif scn.jaw_constraints == 'COWLER':
            col.prop(scn, "cwl_mouth_rot")
            col.prop(scn, "cwl_mouth_loc")
        #Feather constraint & other options(for Cowlers)
        col.label(text = "Cowler Constraints")
        col.prop(scn, "cwl_disable_foot_deform")
        col.prop(scn, "fthr_head_side_constraints")
        col.prop(scn, "fthr_head_top_constraints")
        col.prop(scn, "fthr_hat_constraints")
        col.prop(scn, "fthr_wing_arms_constraints")
        if scn.fthr_wing_arms_constraints:
            col.prop(scn, "fthr_wing_arms_innerAmount")
            col.prop(scn, "fthr_wing_arms_amount")
            for i in range(scn.fthr_wing_arms_amount):
                col.prop(scn, "fthr_wing_arms_influence", index = i, text = ntl(i)+" Fthr Influence:")
        #Deformation controls
        box_deform = box.box()
        col = box_deform.column()
        col.label(text = "Deform Controls:")
        col.prop(scn, "snr_def_control")
        if scn.snr_def_control:
            col.prop(scn, "snr_def_teeth")
            col.prop(scn, "snr_def_tongue")
            col.prop(scn, "snr_def_eye")
            col.prop(scn, "snr_def_eyelid")
            col.prop(scn, "snr_def_backfin")
        col.prop(scn, "def_hat")
        #Custom Property controls
        box_props = box.box()
        col = box_props.column()
        col.label(text = "Property Controls:")
        col.prop(scn, "props_standard")
        col.prop(scn, "props_eye")
        col.label(text = "Color Properties")
        col.prop(scn, "props_colortype", text = "")
        if scn.props_colortype == 'SNARYAN':
            col.prop(scn, "props_basecolor")
            col.prop(scn, "props_secondarycolor")
            col.prop(scn, "props_whitescolor")
            col.prop(scn, "props_darkscolor")
            col.prop(scn, "props_nailscolor")
            col.prop(scn, "props_mouthcolor")
            col.prop(scn, "props_teethcolor")
            col.prop(scn, "props_tonguecolor1")
            col.prop(scn, "props_tonguecolor2")
        elif scn.props_colortype == 'COWLER':
            col.prop(scn, "props_primarycolor")
            col.prop(scn, "props_secondarycolor")
            col.prop(scn, "props_darkscolor")
            col.prop(scn, "props_nailscolor")
            col.prop(scn, "props_mouthcolor")
            col.prop(scn, "props_beakmaincolor")
            col.prop(scn, "props_beaksecondarycolor")
            col.prop(scn, "props_beaktertiarycolor")
            col.prop(scn, "props_tonguecolor1")
            col.prop(scn, "props_tonguecolor2")
        #Generate Rig operators
        col = box.column()


#Extension of Right-click Context Menu
class WM_MT_button_context(bpy.types.Menu):
    bl_label = ""

    # Leave empty for compatibility.
    def draw(self, context):
        pass

def rcmenu_draw(self, context):
    layout = self.layout
    try:    #Check if this is a color property before showing operator
        sel_prop = context.button_prop
        if sel_prop.subtype == 'COLOR':
            layout.operator('view3d.copy_color_driver')
            layout.operator('view3d.paste_color_driver')
    except:
        pass
    layout.operator('view3d.retrieve_attributes')
    try:    #operators used for custom properties regardless of type
        sel_pointer = context.button_pointer    #Retrieve the python data of property parent
        sp_path = sel_pointer.path_from_id()    #path string to pointer property. Attempt to access this property
        layout.operator('view3d.remove_custom_property')
    except: 
        pass
#register and unregister functions
cl_list = (
    CORG_PT_MainPanel, 
    CORG_PT_PropPanel, 
    CORG_PT_CharacterPanel, 
    CORG_PT_PropertyCreationPanel, 
    CORG_PT_ToonControlPanel, 
    CORG_PT_RigGeneration, 
)

def register():
    from bpy.utils import register_class
    for cl in cl_list:
        register_class(cl)
    #Right-click menu: Register menu only if it doesn't already exist.
    rcmenu = getattr(bpy.types, "WM_MT_button_context", None)
    if rcmenu is None:  #Assign the class in case it does not exist already
        register_class(WM_MT_button_context)
        rcmenu = WM_MT_button_context
    # Retrieve a python list for inserting draw functions.
    draw_funcs = rcmenu._dyn_ui_initialize()
    draw_funcs.append(rcmenu_draw)

def unregister():
    from bpy.utils import unregister_class
    for cl in cl_list:
        unregister_class(cl)
    #Remove the appended Right-click Menu options
    rcmenu = getattr(bpy.types, "WM_MT_button_context", None)
    draw_funcs = rcmenu._dyn_ui_initialize()
    draw_funcs.remove(rcmenu_draw)