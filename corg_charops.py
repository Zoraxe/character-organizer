#General Character operators
import bpy
import copy
import mathutils as mu
from .modules.ref_props import *    #Various standard properties
from .modules.mod_rig import *  #rigging functions
from rna_prop_ui import rna_idprop_ui_create as prop_create     #for creating custom properties

pr = bpy.props  #quick reference to blender properties
base_prefix = "char_"
intf_prefix = "chr_"

#For retrieving info from button properties
def dump(obj, text):
    for attr in dir(obj):
        print("%r.%s = %s" % (obj, attr, getattr(obj, attr)))


#Operator to add drivers to colors of character-specific shaders
class CHAR_PT_DriverColors(bpy.types.Operator):
    bl_idname = "view3d." + base_prefix + "driver_colors"
    bl_label = "Add Color Drivers"
    bl_description = """Add drivers to colors on labeled nodes of shader tree. 
        Custom properties for the color must already exist, and they will correspond to nodes based on the property prefix and node label. 
        Nodes that are named accordingly will have their respective color drivers added: 
        """ + snode_names[0] + ", " + snode_names[1] + ", " + snode_names[2] + ", " + snode_names[3]

    @classmethod
    def poll(cls, ctx):
        scn = ctx.scene
        focus_obj = focus_retrieve(scn, scn.focus_set)  #dynamically retrieve the focus object based on the chosen focus_set
        if focus_obj["focus"] != None:   #check if a focus object is selected or not
            return root_check(focus_obj["focus"], focus_obj["root"]) != None    #check for a valid root bone
        else:
            return False

    def execute(self, ctx):
        scn = ctx.scene
        focus_obj = focus_retrieve(scn, scn.focus_set)  #dynamically retrieve the focus object based on the chosen focus_set
        drv_obj = focus_obj["focus"]   #driver object will be the focused rig
        root_bone = root_check(drv_obj, focus_obj["root"])     #retrieve current root bone
        #Create all drivers on marked toon-shading nodes (if there is one)
        mats = ctx.active_object.data.materials     #retrieve material list from currently selected object
        for mat in mats:    #go through each material
            for k in range(len(snode_names)):     #loop through all possible color-node names
                if mat.node_tree.nodes.get(snode_names[k]) is not None:     #node exists
                    node = mat.node_tree.nodes[snode_names[k]]    #sets the node object
                    for j in range(len(snode_props[k])):  #cycle through all props related to node
                        for i in range(4):  #create a driver for every channel in the color
                            drv_add(
                                node.inputs[snode_ind[k][j]], 
                                "default_value", 
                                [drv_obj], 
                                ['pose.bones["' + root_bone.name + '"]["' + snode_props[k][j] + '"][' + str(i) + ']'], 
                                ind = i
                            )
        return {'FINISHED'}

#Operator to add drivers to standard toon shaders
class CHAR_PT_DriverToon(bpy.types.Operator):
    bl_idname = "view3d." + base_prefix + "driver_toon"
    bl_label = "Add Toon Drivers"
    bl_description = """Add toon drivers on labeled nodes of shader tree. 
        The Toon-Shader Base node is used and must be named 'Toon_Main'. 
        Custom properties for the color must already exist, 
        and they will correspond to nodes based on the property name and node label"""

    @classmethod
    def poll(cls, ctx):
        scn = ctx.scene
        focus_obj = focus_retrieve(scn, scn.focus_set)  #dynamically retrieve the focus object based on the chosen focus_set
        if focus_obj["focus"] != None:   #check if a focus object is selected or not
            return root_check(focus_obj["focus"], focus_obj["root"]) != None    #check for a valid root bone
        else:
            return False

    def execute(self, ctx):
        scn = ctx.scene
        focus_obj = focus_retrieve(scn, scn.focus_set)  #dynamically retrieve the focus object based on the chosen focus_set
        drv_obj = focus_obj["focus"]   #driver object will be the focused rig
        root_bone = root_check(drv_obj, focus_obj["root"])     #retrieve current root bone
        tnode_name = "Toon_Main"    #standard name of node to access
        ind_dv = [11, 19, 17, 1, 2, 
            22, 21, 
            3, 4
        ]   #Indices of the node to access
        #Create all drivers on marked toon-shading nodes (if there is one)
        mats = ctx.active_object.data.materials     #retrieve material list from currently selected object
        for mat in mats:    #go through each material
            if mat.node_tree.nodes.get(tnode_name) != None:     #node exists
                tnode = mat.node_tree.nodes[tnode_name]     #toon node
                for i in range(len(toon_prop_names)):    #create a driver for all properties present
                    if toon_prop_names[i] == "TnS_Color":    #create drivers for each index of the color
                        for j in range(4):
                            drv_add(
                                tnode.inputs[ind_dv[i]], 
                                "default_value", 
                                [drv_obj], 
                                ['pose.bones["' + root_bone.name +'"]["'+toon_prop_names[i]+'"]['+str(j)+']'], 
                                ind = j
                            )
                    else:
                        drv_add(
                            tnode.inputs[ind_dv[i]], 
                            "default_value", 
                            [drv_obj], 
                            ['pose.bones["' + root_bone.name + '"]["'+toon_prop_names[i]+'"]']
                        )
        return {'FINISHED'}

#Operator to retrieve property path for bone property to make use of in drivers
class CHAR_PT_CopyColorDriver(bpy.types.Operator):
    bl_idname = "view3d.copy_color_driver"
    bl_label = "Copy as Color Driver"
    bl_description = "Copies the data needed to make a color driver from this property"
    
    @classmethod
    def poll(cls, ctx):
        sel_prop = ctx.button_prop #python data of selected property
        sel_pointer = ctx.button_pointer    #Retrieve the python data of property parent
        #Confirm that the subtype of the property is a color
        try: 
            sp_path = sel_pointer.path_from_id()    #path string to pointer property. Attempt to access this property
            if sel_prop.subtype == 'COLOR':
                return True
            else: 
                return False
        except:
            return False

    def execute(self, ctx):
        scn = ctx.scene     #reference for scene properties
        sel_prop = ctx.button_prop  #Retrieve the python data of selected property
        sel_pointer = ctx.button_pointer    #Retrieve the python data of property parent
        sp_path = sel_pointer.path_from_id()    #path string to pointer property
        #Retrieve the parent object for the property
        pt_path = getattr(ctx, "button_pointer", None)  #button pointer property path
        print("Pointer Object Path: ", repr(pt_path))
        if ".node_tree." in repr(pt_path):  #material color property exception
            sp_path = "node_tree." + sp_path    #append "node_tree" to property path string
            scn.driv_path = sp_path + "." + sel_prop.identifier
        else:   #normal properties
            scn.driv_path = sp_path + '["' + sel_prop.name + '"]'    #property path to use for drivers, saved to scene variable
        rig_path = repr(pt_path)[:len(repr(pt_path)) - len(sp_path) - 1]    #data path string to the rig object containing the property
        scn.driv_rig_path = rig_path    #Store the rig-object path for later use in a driver
        print("Object data path is: ", rig_path)
        print("Color driver path copied as: " + scn.driv_path)

        return {'FINISHED'}

#Operator to paste color drivers from copied values
class CHAR_PT_PasteColorDriver(bpy.types.Operator):
    bl_idname = "view3d.paste_color_driver"
    bl_label = "Paste Color Driver"
    bl_description = "Paste the copied driver data onto all channels of the color"
    
    @classmethod
    def poll(cls, ctx):
        scn = ctx.scene     #reference for scene properties
        sel_prop = ctx.button_prop #python data of selected property
        #Confirm that the subtype of the property is a color and that properties are copied
        try: 
            if sel_prop.subtype == 'COLOR' and scn.driv_path != "" and scn.driv_rig_path != "":
                return True
            else: 
                return False
        except:
            return False

    def execute(self, ctx):
        scn = ctx.scene     #reference for scene properties
        sel_prop = ctx.button_prop  #Retrieve the python data of selected property
        sel_pointer = ctx.button_pointer    #Retrieve the python data of property parent
        drv_obj = eval(scn.driv_rig_path)   #Copied rig object that contains custom property
        if ".materials[" in scn.driv_rig_path:  #check if the object type should be MATERIAL
            id_type = 'MATERIAL'
        else:   #normal object type
            id_type = 'OBJECT'
        for i in range(4):  #create a driver for every channel in the color
            drv_add(
                sel_pointer, 
                "default_value", 
                [drv_obj], 
                [scn.driv_path + "[" + str(i) + "]"], 
                ind = i, id_type = id_type
            )
        return {'FINISHED'}

#Operator to create color custom property with default value and assign it to root bone of focus object
class CHAR_PT_CreateColorProperty(bpy.types.Operator):
    bl_idname = "view3d.create_color_property"
    bl_label = "Create Color Property"
    bl_description = "Create a color property on the root bone of the focus object"

    @classmethod
    def poll(cls, ctx):
        scn = ctx.scene
        focus_obj = focus_retrieve(scn, scn.focus_set)  #dynamically retrieve the focus object based on the chosen focus_set
        if focus_obj["focus"] != None:   #check if a focus object is selected or not
            return root_check(focus_obj["focus"], focus_obj["root"]) != None    #check for a valid root bone
        else:
            return False

    def execute(self, ctx):
        scn = ctx.scene     #scene context reference for retrieving interface-set values
        focus_obj = focus_retrieve(scn, scn.focus_set)  #dynamically retrieve the focus object based on the chosen focus_set
        root_bone = root_check(focus_obj["focus"], focus_obj["root"])
        prop_create(
            root_bone, scn.root_color_name,     
            default = scn.root_color_default,   #set the default color   
            min = 0.0, max = 10.0,      #allow values over 1.0 to get brighter colors
            description = scn.root_color_desc,      #description of the custom property
            subtype = 'COLOR',      #set as a color property
            overridable = True  #Make library-overridable
        )
        return {'FINISHED'}

#Operator to remove custom properties from focus object
class CHAR_PT_RemoveCustomProperty(bpy.types.Operator):
    bl_idname = "view3d.remove_custom_property"
    bl_label = "Remove Custom Property"
    bl_description = "Remove the selected custom property"

    @classmethod
    def poll(cls, ctx):
        sel_prop = ctx.button_prop #python data of selected property
        sel_pointer = ctx.button_pointer    #Retrieve the python data of property parent
        scn = ctx.scene
        focus_obj = focus_retrieve(scn, scn.focus_set)  #dynamically retrieve the focus object based on the chosen focus_set
        if focus_obj["focus"] != None:   #check if a focus object is selected or not
            try: 
                sp_path = sel_pointer.path_from_id()    #path string to pointer property. Attempt to access this property
                #check for a valid root bone (also indirectly confirms focus object is an armature)
                return root_check(focus_obj["focus"], focus_obj["root"]) != None    
            except:
                return False
        else:
            return False
    
    def execute(self, ctx):
        scn = ctx.scene
        sel_prop = ctx.button_prop  #Retrieve the python data of selected property
        sel_pointer = ctx.button_pointer    #Retrieve the python data of property parent
        sp_path = sel_pointer.path_from_id()    #path string to pointer property
        #construct the string that gives the data path to the property starting from bpy.context.
        dp_string = 'scene.obj_focus' + scn.focus_set + '.' + sp_path
        #Remove the property by provided the context-path string and the property name
        bpy.ops.wm.properties_remove(data_path = dp_string, property_name = sel_prop.name)
        return {'FINISHED'}

#Operator to create solidify modifiers, materials, and drivers on all selected objects for toon shading
class CHAR_PT_ToonSolidifyAdd(bpy.types.Operator):
    bl_idname = "view3d.toon_solidify_add"
    bl_label = "Add Toon-Outlines"
    bl_description = "Create solidify modifiers, materials, & drivers linked to the focus-rig that allows for toon-outline shading"

    @classmethod
    def poll(cls, ctx):
        scn = ctx.scene
        focus_obj = focus_retrieve(scn, scn.focus_set)  #dynamically retrieve the focus object based on the chosen focus_set
        #check for focus object and outline-material are set, focus object is a rig, and at least one object is selected
        #return True     #poll override
        if focus_obj["focus"] != None and scn.toon_ol_mat != None:
            if len(ctx.selected_objects) > 0 and root_check(focus_obj["focus"], focus_obj["root"]) != None:
                try:
                    root_bone = root_check(focus_obj["focus"], focus_obj["root"])
                    prop_check = root_bone[to_standard]     #attempt to retrieve the property
                    return True
                except:     #couldn't find toon-outline property
                    return False
            else:   #no objects selected or no valid root bone
                return False
        else:   #No focus object set
            return False

    def execute(self, ctx):
        scn = ctx.scene
        focus_obj = focus_retrieve(scn, scn.focus_set)  #dynamically retrieve the focus object based on the chosen focus_set
        #Input parameters
        if scn.toon_solidify_defaults:  #resort to default settings for string names and the solidify offset if on
            so_name = "Solidify_Outline"
            wn_name = "WeightedNormal"
            sol_offset = 1.0
            sol_override = True
            sol_wn_apply = True
        else:   #use the user-defined settings in the UI
            so_name = scn.toon_solidify_name    #name of Solidify outline modifier
            wn_name = scn.toon_wn_name  #name of weighted normal modifier
            sol_offset = scn.toon_solidify_offset   #Offset value of solidify modifier
            sol_override = scn.toon_solidify_override_os    #Override offset option
            sol_wn_apply = scn.use_weighted_normal  #Option for creating weighted-normal modifier
        thickness = scn.toon_solidify_thickness    #standard thickness for solidify modifier
        so_drv_obj = focus_obj["focus"]   #driver object will be the rig
        root_bone = root_check(focus_obj["focus"], focus_obj["root"])
        so_drv_loc = pb_string(root_bone.name, to_standard)   #property location to point to in the driver
        so_drv_loc_th = pb_string(root_bone.name, to_scale)     #property location for scaling thickness

        mat_ol = scn.toon_ol_mat    #outline material object
        mat_ol_name = mat_ol.name
        objs = ctx.selected_objects     #list of selected objects
        #add the solidify_outline modifier and necessary drivers to all selected objects that do not already have it
        for obj in objs:    #loop through all objects
            try:
                #add the outline material to the last material slot of the object if it is not present
                mats = obj.material_slots  #material slots list
                if len(mats) > 0:
                    if mats[-1].name != mat_ol_name:    #check the last material in the list by nam
                        obj.data.materials.append(mat_ol)   #adds the outlines material to the end
                else:   #no materials on current object
                    obj.data.materials.append(mat_ol)   #adds the outlines material to the end
                mods = obj.modifiers    #list of modifiers on object
                mat_offset = len(mats) - 1  #amount to offset materials by
                #add solidify_outline modifier if there isn't one
                if mods.get(so_name) is None:    #no modifier found
                    solid_mod = mods.new(so_name, 'SOLIDIFY')
                    solid_mod.thickness = thickness     #set the thickness
                    sm_thickness = thickness    #value to use for driver creation
                    solid_mod.offset = sol_offset   #set the offset
                    solid_mod.use_flip_normals = True   #flipped normals are necessary for the outline material to work
                    solid_mod.material_offset = mat_offset
                    solid_mod.material_offset_rim = mat_offset
                else:   #modifier does exist
                    solid_mod = mods[so_name]
                    solid_mod.material_offset = mat_offset
                    solid_mod.material_offset_rim = mat_offset
                    #Override the thickness if requested
                    if scn.toon_solidify_override_th:
                        sm_thickness = thickness    #use given thickness value for driver
                        solid_mod.thickness = thickness
                    else:
                        try:    #attempt to divide by the value of the TnO_Scale value if it exists
                            sm_thickness = copy.deepcopy(solid_mod.thickness)/root_bone[to_scale]   #record the thickness value for use in the new driver
                        except:     #no scale value found. Record the current value of solidify thickness
                            sm_thickness = copy.deepcopy(solid_mod.thickness)
                    #Override the offset if requested
                    if sol_override:
                        solid_mod.offset = sol_offset
                #Add drivers to solidify modifier viewport and render
                solid_mod.driver_remove("show_render")  #remove existing drivers
                solid_mod.driver_remove("show_viewport")
                so_drv1 = drv_add(solid_mod, "show_render", [so_drv_obj], [so_drv_loc])
                so_drv2 = drv_add(solid_mod, "show_viewport", [so_drv_obj], [so_drv_loc])
                #Add driver to control solidify thickness
                solid_mod.driver_remove("thickness")    #Remove any existing drivers
                so_drv_th = drv_add(solid_mod, "thickness", [so_drv_obj], [so_drv_loc_th], [df_name], df_name + " * " + str(solid_mod.thickness))
                #Add weighted normal modifier if there isn't one
                if mods.get(wn_name) is None:   #no modifier found
                    if sol_wn_apply:    #check if use of weighted normal is enabled
                        try:    #not all object types will accept this modifier
                            wn_mod = mods.new(wn_name, 'WEIGHTED_NORMAL')
                        except:
                            pass
                else:   #modifier does exist
                    wn_mod = mods[wn_name]
                    #move the weighted normal to the end of the modifier stack
                    with ctx.temp_override(object = obj):
                        bpy.ops.object.modifier_move_to_index(modifier=wn_name, index=len(mods)-1)
                    #Deprecated Context override
                    # ctxo = ctx.copy()    #Context Override
                    # ctxo['object'] = obj
                    # bpy.ops.object.modifier_move_to_index(ctxo, modifier=wn_name, index=len(mods)-1)
                try:    #not all object types will have this option
                    obj.data.use_auto_smooth = True     #Enable the auto-smooth option on the mesh if it isn't already on
                except:
                    pass
            except: 
                print(obj.name + " is not a valid object to add toon-solidify controls to")
        return {'FINISHED'}

#Operator to remove custom properties from focus object
class CHAR_PT_RigifyGenerateExtra(bpy.types.Operator):
    bl_idname = "view3d.rigify_generate_extra"
    bl_label = "Generate Rig Ex"
    bl_description = "If using Rigify for the rig, re-generate the rig with extra properties and other settings applied (Similar to running a custom Finalize Script that runs after Rigify finishes its generative opertaion). Target rig and other Rigify-specific settings must be specified under the metarig's armature properties (and Rigify add-on must be enabled)"

    @classmethod
    def poll(cls, ctx):     #Determine if the rigify add-on is enabled by trying to extract rigify attribute from the selected metarig
        try:    #attempt to find the Rigify properties
            scn = ctx.scene     #scene reference
            focus_obj = focus_retrieve(scn, scn.focus_set)  #dynamically retrieve the focus object based on the chosen focus_set
            rig_target = bpy.data.armatures[scn.focus_metarig.name].rigify_target_rig
            if rig_target == focus_obj["focus"]:    #make sure the rig target and set focus are the same in order to pull the correct properties
                return True
            else: 
                return False
        except:     #failed to find the properties
            return False

    def execute(self, ctx):     #Use "id_properties_ui()" with the property name string to retrieve custom property object and "as_dict()" to get its settings
        scn = ctx.scene     #scene reference
        focus_obj = focus_retrieve(scn, scn.focus_set)  #dynamically retrieve the focus object based on the chosen focus_set
        #Retrieve all existing custom properties on the root and eye-controller bones
        root_bone = root_check(focus_obj["focus"], focus_obj["root"])   #Retrieve the root pose bone object
        eye_bone = root_check(focus_obj["focus"], focus_obj["eye"], eyeCtrl_name)   #Retrieve the eye_ctrl bone
        bone_tuple = (root_bone, eye_bone)  #tuple of all bones to check properties from
        prop_col = []    #list to hold the key_settings that are generated from each bone
        for bone in bone_tuple:
            key_names = bone.keys()     #Get the list containing all custom property names
            key_settings = []   #list to hold lists of each key property's settings
            for key in key_names: 
                if key not in '_RNA_UI' or key != 'rig_parameters' or key != 'rigify_parameters':     #exclude unnecessary parameters
                    try:    #attempt to retrieve the settings of the listed property
                        key_dict = bone.id_properties_ui(key).as_dict()     #retrieve the settings that make up the custom property
                        #Record the name, default, min, max, description, and subtype for the settings list
                        key_settings.append(
                            [key, key_dict['default'], key_dict['min'], key_dict['max'], key_dict['description'], key_dict['subtype']]
                        )
                    except:     #failed to search properties
                        print("Could not find ui props of " + key)
            prop_col.append(key_settings)    #append to the list for later reference for this specific bone
        prop_col = prop_col[:]  #Make of a copy of the data in the list to change the pointer
        #Regenerate the rig using the Rigify operator, and retrieve the rig object and armature
        mr_viewstatus = [scn.focus_metarig.hide_get(), scn.focus_metarig.hide_viewport]     #Record the view status of the metarig
        mr_viewstatus = mr_viewstatus[:]    #copy the list to remove original pointer association
        scn.focus_metarig.hide_set(False)   #View layer hiding. Metarig must be visbile in viewport in order for rigify operator to work
        scn.focus_metarig.hide_viewport = False     #global visibility
        #Function to append a list of pose bones based on a standardized naming convention. Useful for Cowler feather rigs. 
        def retrieve_pb(app_list, pb_list, prefix_str, suffix_str, start_ind = 0):
            i = start_ind   #iteration counter
            while True:
                try:
                    app_list.append(pb_list[prefix_str + ntl(i) + suffix_str])  #fetch the bone from the pose bone list and append it to the given app_list
                    i += 1  #iterate up one
                except:     #bone not found in the list based on the standard naming
                    print("Could not find " + prefix_str + ntl(i) + suffix_str + " in the pose bone list. Ending loop.")
                    break
            return
        #Pre-generation adjustments for the metarig
        meta_pblist = scn.focus_metarig.pose.bones  #reference to the pose bone list of the metarig
        #Wind-arm feathers, metarig portion
        fwa_meta_ctrl = (("fthrInner.L", "fthrOuter.L"), ("fthrInner.R", "fthrOuter.R"))    #ctrl bones to be referenced in the constraints
        fwa_meta_cst_name = "Copy Rotation Secondary"   #Standard name of secondary rotation constraint on mch feathers (inner or outer control)
        fwa_mchb = [[], []]   #Lists for Wing-arm feathers, mechanical bones in the metarig
        if scn.fthr_wing_arms_constraints:  #Get the list of MCH bones from the metarig and modify the constraint influences with the given interface inputs
            retrieve_pb(fwa_mchb[0], meta_pblist, "MCH_fthr", ".000.L", 0)    #MCH wing bones, left side
            retrieve_pb(fwa_mchb[1], meta_pblist, "MCH_fthr", ".000.R", 0)    #MCH wing bones, right side
            for i in range(len(fwa_mchb)):  #go through both lists
                # if len(fwa_mchb[i]) > 0:    #make sure the list wasn't empty
                for j in range(len(fwa_mchb[i])):   #loop through all bones in the list
                    try:
                        fwa_mchb[i][j].constraints[fwa_meta_cst_name].influence = 1.0 - scn.fthr_wing_arms_influence[j]     #assign the influence corresponding to the values listed in the interface's influence array 
                        fwa_mchb[i][j].constraints[fwa_meta_cst_name].subtarget = fwa_meta_ctrl[i][int(j > scn.fthr_wing_arms_innerAmount)]  #Set the secondary constraint subtarget based on whether or not it's an inner bone or not
                    except:
                        print("Could not find constraint for pose bone " + fwa_mchb[i][j].name + ". Skipping...")
                # else:
                #     print("No mch bones found in list " + str(i+1) + " of the metarig. Skipping...")
        #Generate the Rigify rig using the standard generation operator
        with ctx.temp_override(object = scn.focus_metarig): #Override the object with the metarig before executing the operator
            bpy.ops.pose.rigify_generate()  #Regenerates the rig according to the chosen rigify settings on the metarig (and if the context for the op is correct)
        mrig_name = scn.focus_metarig.name  #get the name of the metarig
        rig_name = bpy.data.armatures[mrig_name].rigify_target_rig.name     #get the name of the rig
        rig = bpy.data.objects[rig_name]    #get the rig object
        ra = bpy.data.armatures[rig_name]   #get armature object reference
        pblist = rig.pose.bones     #quick reference to the rig's pose bone list
        #Create custom constraints
        #Jaw Constraints
        if scn.jaw_constraints == 'SNARYAN':    #Snaryan Jaw Constraints
            #create a transform constraint between the jaw and lower lip
            lip_bone = pblist["lip.B"]  #bottom lip ctrl reference
            jaw_bone = pblist["jaw_master"]     #jaw ctrl bone reference
            lip_cst = rot_loc(rig, "Lip_Shrink", lip_bone, jaw_bone)
            #create a transform constraint between the jaw and tongue end
            tongue_bone = pblist.bones["tongue"]
            tongue_cst = rot_loc(rig, "Tongue_Shrink", tongue_bone, jaw_bone, loc_dir = 'Y', 
                rot = scn.snr_mouth_rot, loc = scn.snr_mouth_loc)
        elif scn.jaw_constraints == 'COWLER':   #Cowler Jaw Constraints
            #create a transform constraint between the jaw and lower lip
            lip_bone = pblist["lip.B.000"]  #bottom lip ctrl reference
            lip_boneL = pblist["lip.B.L.003"]   #bottom lip, left side
            lip_boneR = pblist["lip.B.R.003"]   #bottom lip, right side
            jaw_bone = pblist["jaw_master"]     #jaw ctrl bone reference
            lip_cst = rot_loc(rig, "Lip_Shrink", lip_bone, jaw_bone)
            lip_cst = rot_loc(rig, "Lip_Shrink", lip_boneL, jaw_bone, 
                rot = scn.cwl_mouth_rot, loc = scn.cwl_mouth_loc)
            lip_cst = rot_loc(rig, "Lip_Shrink", lip_boneR, jaw_bone, 
                rot = scn.cwl_mouth_rot, loc = scn.cwl_mouth_loc)
            #create a transform constraint between the jaw and tongue end
            try:
                tongue_bone = pblist["tongue"]
            except: 
                tongue_bone = pblist["tongue.005"]
                tongue_cst = rot_loc(rig, "Tongue_Shrink", tongue_bone, jaw_bone, loc_dir = 'Y', 
                    rot = scn.cwl_mouth_rot, loc = scn.cwl_mouth_loc)
        #Eye Constraints
        if scn.eye_constraints == 'SNARYAN':     #Snaryan eye constraint implementation
            #create transform constraints for eyelid controllers
            el_ctrl_l = pblist["CTRL-eyelid_T.L"]
            el_ctrl_r = pblist["CTRL-eyelid_T.R"]
            el_top_l = pblist["eyelid_T1.L.003"]
            el_top_r = pblist["eyelid_T1.R.003"]
            el_cst_name = "Eyelid_Open"
            eltl_cst = loc_loc(rig, el_cst_name, el_top_l, el_ctrl_l, ext = True, 
                loc1 = scn.snr_eyelid_loc1, loc2 = scn.snr_eyelid_loc2)
            eltr_cst = loc_loc(rig, el_cst_name, el_top_r, el_ctrl_r, ext = True, 
                loc1 = scn.snr_eyelid_loc1, loc2 = scn.snr_eyelid_loc2)
        elif scn.eye_constraints == 'COWLER':   #Cowler eye constraint implementation
            #create transform constraints for eyelid controllers, Cowler-specific
            el_ctrl_l = pblist["CTRL-eyelid_T.L"]
            el_ctrl_r = pblist["CTRL-eyelid_T.R"]
            el_top_l = pblist["eyelid_T1.L.007"]
            el_top_r = pblist["eyelid_T1.R.007"]
            el_cst_name = "Eyelid_Open"
            eltl_cst = loc_loc(rig, el_cst_name, el_top_l, el_ctrl_l, ext = True)
            eltr_cst = loc_loc(rig, el_cst_name, el_top_r, el_ctrl_r, ext = True)
        #Feather Constraints (Typically for Cowlers)
        fhs_curlL, fhs_curlR = "HeadFthr_Curl.L", "HeadFthr_Curl.R"     #Standard names of the curl control bones, head-side feathers
        fht_curl = "headT_fthr_Curl"    #Standard names of the curl control bone, head-top feathers
        fhat_curl = "hat_fthr_Curl"     #Standard names of the curl control bone, hat feathers
        fhs_pbL, fhs_pbR, fht_pb, fhat_pb = [], [], [], []   #Various pose bone lists start off empty
        hfthr_cst_name = "Copy Rotation Curl"   #constraint name for head/hat feathers
        if scn.fthr_head_side_constraints:  #Head side feathers
            #Retrieve the left & right sides as separate lists
            retrieve_pb(fhs_pbL, pblist, "head_fthr", ".000.L", 0)
            retrieve_pb(fhs_pbR, pblist, "head_fthr", ".000.R", 0)
            #Retrieve control pose bones and assign the constraint to all pose bones in their respective groups
            fhs_curlL_ctrl, fhs_curlR_ctrl = pblist[fhs_curlL], pblist[fhs_curlR]   #ctrl pose bones
            fhs_cstL, fhs_cstR = [None]*len(fhs_pbL), [None]*len(fhs_pbR)   #lists to hold the constraints
            for i in range(len(fhs_pbL)):
                fhs_cstL[i] = rot_const(rig, hfthr_cst_name, fhs_pbL[i], fhs_curlL_ctrl, 1.0)
            for i in range(len(fhs_pbR)):
                fhs_cstR[i] = rot_const(rig, hfthr_cst_name, fhs_pbR[i], fhs_curlR_ctrl, 1.0)
        if scn.fthr_head_top_constraints:   #Head top feathers
            #Retrieve the middle, left, & right sides in a single list
            retrieve_pb(fht_pb, pblist, "headT_fthr", ".000", 0)
            retrieve_pb(fht_pb, pblist, "headT_fthr", ".000.L", 1)
            retrieve_pb(fht_pb, pblist, "headT_fthr", ".000.R", 1)
            #Retrieve the control pose bone and assign the constraint to all bones
            fht_curl_ctrl = pblist[fht_curl]
            fht_cst = [None]*len(fht_pb)
            for i in range(len(fht_pb)):
                fht_cst = rot_const(rig, hfthr_cst_name, fht_pb[i], fht_curl_ctrl, 1.0)
        if scn.fthr_hat_constraints:    #Hat feathers
            #Retrieve the hat feathers in one list
            retrieve_pb(fhat_pb, pblist, "hat_fthr", ".000", 0)
            #Retrieve the control pose bone and assign the constraint to all bones
            fhat_curl_ctrl = pblist[fhat_curl]
            fhat_cst = [None]*len(fhat_pb)
            for i in range(len(fhat_pb)):
                fhat_cst = rot_const(rig, hfthr_cst_name, fhat_pb[i], fhat_curl_ctrl, 1.0)
        #Wing-arm feathers
        fwa_curl = (   #Curl control bone names
            ("fthrOuter_Curl.L", "fthrInner_Curl.L", "fthrMid_Curl.L"), 
            ("fthrOuter_Curl.R", "fthrInner_Curl.R", "fthrMid_Curl.R")
        )
        fwa_cst_names = ("Copy Rotation Middle", "Copy Rotation Secondary")
        fwa_pb = [[], []]   #List to store both left & right feather pose bones to receive constraints
        if scn.fthr_wing_arms_constraints:  #Get the list of wing-arm feather pose bones and apply the constraints
            retrieve_pb(fwa_pb[0], pblist, "fthr", ".000.L", 0)    #wing bones, left side
            retrieve_pb(fwa_pb[1], pblist, "fthr", ".000.R", 0)    #wing bones, right side
            #Retrieve the control pose bones and assign rotation constraints with the weighted influences
            fwa_io_cst = [[None]*len(fwa_pb[0]), [None]*len(fwa_pb[1])]     #list to store all inner and outer bone constraints
            fwa_mid_cst = [[None]*len(fwa_pb[0]), [None]*len(fwa_pb[1])]    #list to store all middle bone constraints
            for i in range(len(fwa_curl)):  #both left and right sides
                for j in range(len(fwa_pb[i])):
                    inside_check = int(j <= scn.fthr_wing_arms_innerAmount)     #checks if this bone should be assigned with influence of the inside or outer control bone
                    try:    #create the constraints between the relavant pose bones also keeping in mind the inner and outer checks
                        fwa_io_cst[i][j] = rot_const(rig, fwa_cst_names[inside_check], fwa_pb[i][j], pblist[fwa_curl[i][inside_check]], 1.0 - scn.fthr_wing_arms_influence[j])
                        fwa_mid_cst[i][j] = rot_const(rig, fwa_cst_names[0], fwa_pb[i][j], pblist[fwa_curl[i][2]], scn.fthr_wing_arms_influence[j])
                    except:     #constraint couldn't be made for any reason
                        print("Could not properly create curl constraints for pose bone " + fwa_pb[i][j].name + ". Skipping...")
        #Disable feet deform bones. Used for Cowler rigs where other bones supersede their use
        if scn.cwl_disable_foot_deform:     #check if the option is enabled
            ra.bones["DEF-toe.L"].use_deform = False
            ra.bones["DEF-toe.R"].use_deform = False
            ra.bones["DEF-foot.L"].use_deform = False
            ra.bones["DEF-foot.R"].use_deform = False

        #Add the selected groups of custom properties
        root_props = [[], [], [], [], [], []]   #names, defaults, mins, maxes, descriptions, subtypes
        eye_props = [[], [], [], [], [], []]
        #Standard character properties
        if scn.props_standard:
            for i in range(len(root_props)):
                root_props[i] = [*root_props[i], *root_prop_ext[i]]    #append the root properties to each list
        #Snaryan properties
        if scn.props_colortype == 'SNARYAN':    #Add Snaryan color properties
            for i in range(len(root_props)):
                if i == 1:  #Use user-inputs for default properties
                    root_props[i] = [*root_props[i], 
                        scn.props_basecolor, 
                        scn.props_secondarycolor, 
                        scn.props_whitescolor, 
                        scn.props_darkscolor, 
                        scn.props_nailscolor, 
                        scn.props_mouthcolor, 
                        scn.props_teethcolor, 
                        scn.props_tonguecolor1, 
                        scn.props_tonguecolor2
                    ]
                else: 
                    root_props[i] = [*root_props[i], *snaryan_prop_ext[i]]
        #Cowler properties
        if scn.props_colortype == 'COWLER':    #Add Cowler color properties
            for i in range(len(root_props)):
                if i == 1:  #Use user-inputs for default properties
                    root_props[i] = [*root_props[i], 
                        scn.props_primarycolor, 
                        scn.props_secondarycolor, 
                        scn.props_darkscolor, 
                        scn.props_nailscolor, 
                        scn.props_mouthcolor, 
                        scn.props_beakmaincolor, 
                        scn.props_beaksecondarycolor, 
                        scn.props_beaktertiarycolor, 
                        scn.props_tonguecolor1, 
                        scn.props_tonguecolor2
                    ]
                else: 
                    root_props[i] = [*root_props[i], *cowler_prop_ext[i]]
        #Eye properties
        if scn.props_eye:
            for i in range(len(eye_props)): 
                eye_props[i] = [*eye_props[i], *eye_prop_ext[i]]
        #Redo data paths for bone tuple since they have been broken by the rig re-generation
        bone_tuple = (root_check(focus_obj["focus"], focus_obj["root"]), root_check(focus_obj["focus"], focus_obj["eye"], eyeCtrl_name))
        #Add the properties to the root
        for i in range(len(root_props[0])):
            prop_create(bone_tuple[0], root_props[0][i], default = root_props[1][i], 
                min = root_props[2][i], max = root_props[3][i], 
                description = root_props[4][i], 
                subtype = root_props[5][i], 
                overridable = True
            )
        #Add the properties to the eye controller
        for i in range(len(eye_props[0])):
            prop_create(bone_tuple[1], eye_props[0][i], default = eye_props[1][i], 
                min = eye_props[2][i], max = eye_props[3][i], 
                description = eye_props[4][i], 
                subtype = eye_props[5][i], 
                overridable = True
            )
        #Go through the bone tuple and re-add the custom properties that have not already been added
        for i in range(len(bone_tuple)):
            key_names = bone_tuple[i].keys()     #list of current custom property names
            for j in range(len(prop_col[i])):    #cycle through all entries recorded in prop_collection for this bone
                if prop_col[i][j][0] not in key_names:   #retrieve the prop-name and compare against the key-list
                    prop_create(bone_tuple[i], prop_col[i][j][0], default = prop_col[i][j][1],
                        min = prop_col[i][j][2], max = prop_col[i][j][3], 
                        description = prop_col[i][j][4], 
                        subtype = prop_col[i][j][5], 
                        overridable = True
                    )
        #Adjust the visual rotation of the hand_ik controllers
        hand_ik = (pblist["hand_ik.L"], pblist["hand_ik.R"])
        for bone in hand_ik:
            bone.custom_shape_rotation_euler[1] = m.pi/2     #90 deg rotation 
        #Change the Deform option on deformation bones based on chosen settings
        #Snaryan-specific deform options
        snr_def_bones = []  #start with empty list
        if scn.snr_def_control:     #only perform this operation if the def-control option is enabled
            if not scn.snr_def_teeth:   #teeth deform bones
                snr_def_bones = [*snr_def_bones, *snr_def_teeth]
            if not scn.snr_def_tongue:   #tongue deform bones
                snr_def_bones = [*snr_def_bones, *snr_def_tongue]
            if not scn.snr_def_eye:   #eye deform bones
                snr_def_bones = [*snr_def_bones, *snr_def_eye]
            if not scn.snr_def_eyelid:   #eyelid deform bones
                snr_def_bones = [*snr_def_bones, *snr_def_eyelid]
            if not scn.snr_def_backfin:   #backfin deform bones
                snr_def_bones = [*snr_def_bones, *snr_def_backfin]
            if len(snr_def_bones) != 0:     #check for empty list
                for bname in snr_def_bones:     #cycle through all bones, if there are any
                    try:    #attempt to find the bone object and change its deform option
                        ra.bones[bname].use_deform = False
                    except:
                        print("Deform bone " + bname + "was not found. Skipping...")
        #General deform options
        gen_def_bones = []  #start with empty list
        if not scn.def_hat:     #hat deform bone
            gen_def_bones = [*gen_def_bones, *def_hat]
        if len(gen_def_bones) != 0:     #check for empty list
            for bname in gen_def_bones:     #cycle through all bones, if there are any
                try:    #attempt to find the bone object and change its deform option
                    ra.bones[bname].use_deform = False
                except:
                    print("Deform bone " + bname + "was not found. Skipping...")
        #Reset the view status of the metarig
        scn.focus_metarig.hide_set(mr_viewstatus[0])   #View layer hiding
        scn.focus_metarig.hide_viewport = mr_viewstatus[1]     #global visibility
        return {'FINISHED'}

#Operator to add selected property-groups to root bone if they do not already exist
class CHAR_PT_AddStandardProperties(bpy.types.Operator):
    bl_idname = "view3d.add_standard_properties"
    bl_label = "Add Standard Properties"
    bl_description = "Add the selected property groups to the rig's root bone. Checks will be done to make sure no duplicate properties are created if they already exist"

    @classmethod
    def poll(cls, ctx):
        scn = ctx.scene
        focus_obj = focus_retrieve(scn, scn.focus_set)  #dynamically retrieve the focus object based on the chosen focus_set
        if focus_obj["focus"] != None:   #check if a focus object is selected or not
            return root_check(focus_obj["focus"], focus_obj["root"]) != None or root_check(focus_obj["focus"], focus_obj["eye"], eyeCtrl_name)   #check for a valid root bone or eye bone
        else:
            return False

    def execute(self, ctx):
        scn = ctx.scene     #scene reference
        focus_obj = focus_retrieve(scn, scn.focus_set)  #dynamically retrieve the focus object based on the chosen focus_set
        root_bone = root_check(focus_obj["focus"], focus_obj["root"])   #Retrieve the root pose bone object
        eye_bone = root_check(focus_obj["focus"], focus_obj["eye"], eyeCtrl_name)   #Retrieve the eye-control pose bone object
        #Add the selected groups of custom properties
        root_props = [[], [], [], [], [], []]   #names, defaults, mins, maxes, descriptions, subtypes
        eye_props = [[], [], [], [], [], []]
        #Standard character properties
        if root_bone != None:   #check if bone is present
            for i in range(len(root_props)):
                root_props[i] = [*root_props[i], *root_prop_ext[i]]    #append the root properties to each list
            #Add the properties to the root
            key_names = root_bone.keys()     #list of current custom property names
            for i in range(len(root_props[0])):
                if root_props[0][i] not in key_names:   #retrieve the prop-name and compare against the key-list
                    prop_create(root_bone, root_props[0][i], default = root_props[1][i], 
                        min = root_props[2][i], max = root_props[3][i], 
                        description = root_props[4][i], 
                        subtype = root_props[5][i], 
                        overridable = True
                    )
        if eye_bone != None:    #check if bone is present
            for i in range(len(eye_props)):
                eye_props[i] = [*eye_props[i], *eye_prop_ext[i]]
            #Add the properties to the eye controller
            for i in range(len(eye_props[0])):
                prop_create(eye_bone, eye_props[0][i], default = eye_props[1][i], 
                    min = eye_props[2][i], max = eye_props[3][i], 
                    description = eye_props[4][i], 
                    subtype = eye_props[5][i], 
                    overridable = True
                )
                
        return {'FINISHED'}

#Operator to set all properties within the selected rig back to default
class CHAR_PT_ResetDefaultProperties(bpy.types.Operator):
    """Confirm resetting all the properties displayed here to their defaults. This will override the current property settings"""
    bl_idname = "view3d.reset_default_properties"
    bl_label = "Reset Properties to Default"
    bl_description = "Reset all properties of the set root and eye bones to default"

    @classmethod
    def poll(cls, ctx):
        scn = ctx.scene
        focus_obj = focus_retrieve(scn, scn.focus_set)  #dynamically retrieve the focus object based on the chosen focus_set
        if focus_obj["focus"] != None:   #check if a focus object is selected or not
            return root_check(focus_obj["focus"], focus_obj["root"]) != None or root_check(focus_obj["focus"], focus_obj["eye"], eyeCtrl_name)   #check for a valid root bone or eye bone
        else:
            return False
    
    def execute(self, ctx):
        self.report({'INFO'}, "Default property values restored on selected rig")
        scn = ctx.scene     #scene reference
        focus_obj = focus_retrieve(scn, scn.focus_set)  #dynamically retrieve the focus object based on the chosen focus_set
        root_bone = root_check(focus_obj["focus"], focus_obj["root"])   #Retrieve the root pose bone object
        eye_bone = root_check(focus_obj["focus"], focus_obj["eye"], eyeCtrl_name)   #Retrieve the eye-control pose bone object
        if root_bone != None:   #check if bone is present
            root_keys = root_bone.keys()
            for key in root_keys:
                try:
                    prop = root_bone.id_properties_ui(key)  #property object
                    default_val = prop.as_dict()['default']     #retrieve the default value
                    root_bone[key] = default_val    #set to the default value
                except:
                    print(key + " at " + root_bone.name + "invalid for setting to default value")
        if eye_bone != None:   #check if bone is present
            eye_keys = eye_bone.keys()
            for key in eye_keys:
                try:
                    prop = eye_bone.id_properties_ui(key)  #property object
                    default_val = prop.as_dict()['default']     #retrieve the default value
                    eye_bone[key] = default_val    #set to the default value
                except:
                    print(key + " at " + eye_bone.name + "invalid for setting to default value")

        return {'FINISHED'}

    def invoke(self, context, event):
        return context.window_manager.invoke_confirm(self, event)

#Debugging: Operator to retrieve attributes of selected property
class CHAR_PT_RetrieveAttributes(bpy.types.Operator):
    bl_idname = "view3d.retrieve_attributes"
    bl_label = "Retrieve Attributes"
    bl_description = "Retrieve all attributes of selected property"

    @classmethod
    def poll(cls, ctx):
        return True

    def execute(self, ctx):
        scn = ctx.scene     #reference for scene properties
        sel_prop = ctx.button_prop  #Retrieve the python data of selected property
        sel_pointer = ctx.button_pointer    #Retrieve the python data of property parent
        sp_path = sel_pointer.path_from_id()    #path string to pointer property
        #Retrieve the parent object for the property
        pt_path = getattr(ctx, "button_pointer", None)  #button pointer property path
        prop_path = getattr(ctx, "button_prop", None)   #button property path
        dump(pt_path, "button_pointer")
        dump(prop_path, "button_prop")
        return {'FINISHED'}

#register and unregister functions
cl_list = (
    CHAR_PT_DriverColors, 
    CHAR_PT_DriverToon, 
    CHAR_PT_CopyColorDriver, 
    CHAR_PT_PasteColorDriver, 
    CHAR_PT_CreateColorProperty, 
    CHAR_PT_RemoveCustomProperty, 
    CHAR_PT_ToonSolidifyAdd, 
    CHAR_PT_RigifyGenerateExtra, 
    CHAR_PT_AddStandardProperties, 
    CHAR_PT_RetrieveAttributes, 
    CHAR_PT_ResetDefaultProperties, 
)

def register():
    from bpy.utils import register_class
    for cl in cl_list:
        register_class(cl)

def unregister():
    from bpy.utils import unregister_class
    for cl in cl_list:
        unregister_class(cl)