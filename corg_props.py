#Define all the scene properties here
import bpy
import math as m
from bpy.props import *
from .modules.ref_props import *    #Import all reference properties

# def filter_callback(self, object):
#         return object.name in self.my_collection.objects.keys()
def filter_focus(self, object):     #check if the object is an armature type
    return object.type == 'ARMATURE'

#Create properties for the scene to be used from the panel
def init_properties():
    prop_sc = bpy.types.Scene   #Scene property reference
    float_min = 0.000001     #standard minimum float for non-zero values
    scale_adj = 1.0   #Scale adjustment for all values

    ####################
    # Variable Properties
    ####################
    #Enumerated property to set what focus object to pull from
    prop_sc.focus_set = EnumProperty(
        items = [
            ('1', "1", "Rig Focus 1", 1), 
            ('2', "2", "Rig Focus 2", 2), 
            ('3', "3", "Rig Focus 3", 3), 
            ('4', "4", "Rig Focus 4", 4), 
            ('5', "5", "Rig Focus 5", 5), 
        ], 
        name = "Focus Set", 
        description = "Set the focus object to pull objects and show properties for. Also will switch which armature rig operators will be applied to", 
        default = 1, 
    )
    #Pointer Objects to pull properties from
    prop_sc.obj_focus1 = PointerProperty(
        name = "Obj Focus 1", 
        description = "Select the object to display properties and controls from",  
        type = bpy.types.Object, 
        poll = filter_focus, 
    )
    prop_sc.obj_focus2 = PointerProperty(
        name = "Obj Focus 2", 
        description = "Select the object to display properties and controls from",  
        type = bpy.types.Object, 
        poll = filter_focus, 
    )
    prop_sc.obj_focus3 = PointerProperty(
        name = "Obj Focus 3", 
        description = "Select the object to display properties and controls from",  
        type = bpy.types.Object, 
        poll = filter_focus, 
    )
    prop_sc.obj_focus4 = PointerProperty(
        name = "Obj Focus 4", 
        description = "Select the object to display properties and controls from",  
        type = bpy.types.Object, 
        poll = filter_focus, 
    )
    prop_sc.obj_focus5 = PointerProperty(
        name = "Obj Focus 5", 
        description = "Select the object to display properties and controls from",  
        type = bpy.types.Object, 
        poll = filter_focus, 
    )
    #String property, property to store for driver usage
    prop_sc.driv_path = StringProperty(
        name = "Driver Path String", 
        description = "Property path to use for creating drivers", 
        default = "", 
    )
    #String property, data path to rig object used in drivers
    prop_sc.driv_rig_path = StringProperty(
        name = "Driver Rig-Path String", 
        description = "Object data path to rig for use in drivers", 
        default = "", 
    )
    #String property, to store active bone name for root
    prop_sc.active_root_name = StringProperty(
        name = "Active Root Name", 
        description = "Property to store active name of root bone in use", 
        default = root_name[0], 
    )
    #Pointer property, metarig selection
    prop_sc.focus_metarig = PointerProperty(
        name = "Metarig Target", 
        description = "Select the Rigify metarig to generate from", 
        type = bpy.types.Object, 
        poll = filter_focus, 
    )

    ####################
    # Menu Properties
    ####################
    #String property, to allow for custom root bone to be defined
    prop_sc.obj_focus1_root = StringProperty(
        name = "Root Name 1", 
        description = "Enter the name of the root bone you wish to track properties from. This will override any default names for the root bone", 
        default = "", 
    )
    prop_sc.obj_focus2_root = StringProperty(
        name = "Root Name 2", 
        description = "Enter the name of the root bone you wish to track properties from. This will override any default names for the root bone", 
        default = "", 
    )
    prop_sc.obj_focus3_root = StringProperty(
        name = "Root Name 3", 
        description = "Enter the name of the root bone you wish to track properties from. This will override any default names for the root bone", 
        default = "", 
    )
    prop_sc.obj_focus4_root = StringProperty(
        name = "Root Name 4", 
        description = "Enter the name of the root bone you wish to track properties from. This will override any default names for the root bone", 
        default = "", 
    )
    prop_sc.obj_focus5_root = StringProperty(
        name = "Root Name 5", 
        description = "Enter the name of the root bone you wish to track properties from. This will override any default names for the root bone", 
        default = "", 
    )
    #String property, to allow for custom eye bone to be defined
    prop_sc.obj_focus1_eye = StringProperty(
        name = "Eye CTRL Name 1", 
        description = "Enter the name of the eye-control bone you wish to track properties from. This will override any default names for the eye-control bone", 
        default = "", 
    )
    prop_sc.obj_focus2_eye = StringProperty(
        name = "Eye CTRL Name 2", 
        description = "Enter the name of the eye-control bone you wish to track properties from. This will override any default names for the eye-control bone", 
        default = "", 
    )
    prop_sc.obj_focus3_eye = StringProperty(
        name = "Eye CTRL Name 3", 
        description = "Enter the name of the eye-control bone you wish to track properties from. This will override any default names for the eye-control bone", 
        default = "", 
    )
    prop_sc.obj_focus4_eye = StringProperty(
        name = "Eye CTRL Name 4", 
        description = "Enter the name of the eye-control bone you wish to track properties from. This will override any default names for the eye-control bone", 
        default = "", 
    )
    prop_sc.obj_focus5_eye = StringProperty(
        name = "Eye CTRL Name 5", 
        description = "Enter the name of the eye-control bone you wish to track properties from. This will override any default names for the eye-control bone", 
        default = "", 
    )
    #Boolean property, to track if a valid root bone is being tracked from the focus object or not
    prop_sc.root_valid = BoolProperty(
        name = "Root Valid", 
        description = "Property that tracks if a valid root bone is available to reference from on the focus object", 
        default = False, 
    )
    #Color property, for setting the default color for a new custom color property to be assigned
    prop_sc.root_color_default = FloatVectorProperty(
        name = "Root Color Default", 
        description = "Set the default color to be used when creating a new custom property to be assigned to the root bone", 
        size = 4, 
        default = (1.0, 1.0, 1.0, 1.0), 
        subtype = 'COLOR', 
        min = 0.0, 
        max = 10.0, 
    )
    #String property, set the name to use for a new custom color property on root bone
    prop_sc.root_color_name = StringProperty(
        name = "Root Color Name", 
        description = "Set the name of the color property to be assigned to the root bone. Make sure to enter a prefix to properly categorize the property. Example prefixes are 'Clth_' for clothing, 'Tn_' or 'TnO_' for toon properties, 'Snr_' for Snaryan, etc.",
        default = "",  
    )
    #String property, set the description to use for a new custom color property on root bone
    prop_sc.root_color_desc = StringProperty(
        name = "Root Color Description", 
        description = "Set the description string for the color property to be assigned to the root bone", 
        default = "Set the color for this property", 
    )
    #Pointer Material for toon-solidify material
    prop_sc.toon_ol_mat = PointerProperty(
        name = "Toon Outline Material", 
        description = "Select the material that will make up the toon outline when used with the appropriate solidify modifier", 
        type = bpy.types.Material, 
    )
    #Float Property, set the toon-solidify thickness
    prop_sc.toon_solidify_thickness = FloatProperty(
        name = "Toon Solidify Thickness", 
        description = "Set the thickness to use in the toon-outline solidify modifier", 
        min = 0.0, 
        default = 0.01, 
        unit = 'LENGTH', 
    )
    #Float Property, set the toon-solidify offset
    prop_sc.toon_solidify_offset = FloatProperty(
        name = "Toon Solidify Offset", 
        description = "Set the offset of the toon-outline solidify modifier upon creation", 
        min = -1.0, 
        max = 1.0, 
        default = 1.0, 
    )
    #String Property, set the toon-outline solidify-modifier name
    prop_sc.toon_solidify_name = StringProperty(
        name = "Solidify Modifier Name", 
        description = "Set the name of the toon-outline solidify modifier. Recommended to keep as the default unless otherwise needed", 
        default = "Solidify_Outline", 
    )
    #String Property, set the toon-outline weighted normal name
    prop_sc.toon_wn_name = StringProperty(
        name = "Weighted Normal Modifier Name", 
        description = "Set the name of the toon-outline weighted normal modifier. Recommended to keep as the default unless otherwise needed", 
        default = "WeightedNormal", 
    )
    #Boolean Property, Option to enable extra options for toon-solidify operator
    prop_sc.toon_solidify_defaults = BoolProperty(
        name = "Toon Defaults", 
        description = "Option to toggle the use of default modifier names and offset settings for the toon-solidify modifiers", 
        default = True,  
    )
    #Boolean Property, Option to override thickness on selected object that toon-solidify is applied to
    prop_sc.toon_solidify_override_th = BoolProperty(
        name = "Override Thickness", 
        description = "Option to override the thickness with the value set here for any toon-solidify modifiers that already exist", 
        default = True, 
    )
    #Boolean Property, Option to override offset on selected object that toon-solidify is applied to
    prop_sc.toon_solidify_override_os = BoolProperty(
        name = "Override Offset", 
        description = "Option to override the offset with the value set here for any toon-solidify modifiers that already exist", 
        default = True, 
    )
    #Boolean Property, Option to enable or disable the use of a weighted normal modifier within the toon-solidify operator
    prop_sc.use_weighted_normal = BoolProperty(
        name = "Use Weighted Normal", 
        description = "Option to enabled or disable the creation of the weighted_normal modifier when using the toon-solidify operator", 
        default = True, 
    )

    ############################
    # Character settings
    ############################
    #Typical Eye&Jaw Constraints
    prop_sc.eye_constraints = EnumProperty(     #Eye Constraint Options
        items = [
            ('NONE', "None", "No special eye constraints nor controls", 1), 
            ('SNARYAN', "Snaryan", "Snaryan-type eye constraints", 2), 
            ('COWLER', "Cowler", "Cowler-type eye constraints", 3), 
        ], 
        name = "Eye Constraints", 
        description = "Option to apply typical eye constraints upon generating the rig. Useful for standard character types", 
        default = 1, 
    )
    prop_sc.snr_eyelid_loc1 = FloatProperty(    #Snaryan Eyelid controller parameter
        name = "Eyelid CTRL Travel", 
        description = "Set the travel distance for the eyelid controller when creating the eyelid's transformation constraint", 
        default = 0.1, 
    )
    prop_sc.snr_eyelid_loc2 = FloatProperty(    #Snaryan Eyelid parameter
        name = "Eyelid Travel", 
        description = "Set the travel distance for the eyelid when creating the eyelid's transformation constraint", 
        default = 0.1, 
    )
    prop_sc.cwl_eyelid_loc1 = FloatProperty(    #Cowler Eyelid controller parameter
        name = "Eyelid CTRL Travel", 
        description = "Set the travel distance for the eyelid controller when creating the eyelid's transformation constraint", 
        default = 0.1, 
    )
    prop_sc.cwl_eyelid_loc2 = FloatProperty(    #Cowler Eyelid parameter
        name = "Eyelid Travel", 
        description = "Set the travel distance for the eyelid when creating the eyelid's transformation constraint", 
        default = 0.1, 
    )
    prop_sc.jaw_constraints = EnumProperty(     #Jaw Constraint Options
        items = [
            ('NONE', "None", "No special jaw constraints nor controls", 1), 
            ('SNARYAN', "Snaryan", "Snaryan-type jaw constraints", 2), 
            ('COWLER', "Cowler", "Cowler-type jaw constraints", 3), 
        ], 
        name = "Jaw Constraints", 
        description = "Option to apply typical jaw constraints upon generating the rig. Useful for standard character types", 
        default = 1, 
    )
    prop_sc.snr_mouth_rot = FloatProperty(  #Snaryan controller rotation parameter
        name = "Jaw CTRL Rotation", 
        description = "Rotation amount of the jaw control within the bottom-lip transformation constraint. This sets the range of rotation for the jaw affecting the retraction of the bottom lip", 
        default = m.pi/12, 
        unit = 'ROTATION',
    )
    prop_sc.snr_mouth_loc = FloatProperty(  #Snaryan mouth translation parameter
        name = "Lower Lip Retraction", 
        description = "Set the amount the lower lip should retract when the jaw is opened. This is done through a transformation constraint linking the two", 
        default = -0.075, 
    )
    prop_sc.cwl_mouth_rot = FloatProperty(  #Cowler controller rotation parameter
        name = "Jaw CTRL Rotation", 
        description = "Rotation amount of the jaw control within the bottom-lip transformation constraint. This sets the range of rotation for the jaw affecting the retraction of the bottom lip", 
        default = m.pi/9, 
        unit = 'ROTATION',
    )
    prop_sc.cwl_mouth_loc = FloatProperty(  #Cowler mouth translation parameter
        name = "Lower Lip Retraction", 
        description = "Set the amount the lower lip should retract when the jaw is opened. This is done through a transformation constraint linking the two", 
        default = -0.04, 
    )
    #Feather Constraint Properties (For Cowlers)
    prop_sc.fthr_head_side_constraints = BoolProperty(  #Head-side feather constraint option
        name = "Feather Head-Side Constraints", 
        description = "Toggle the use of automatic constraints for head-side feathers. Must use the specific naming convention: head_fthr[Letter, alphabetical].000.[L or R]", 
        default = False, 
    )
    prop_sc.fthr_head_top_constraints = BoolProperty(  #Head-top feather constraint option
        name = "Feather Head-Top Constraints", 
        description = "Toggle the use of automatic constraints for head-top feathers. Must use the specific naming convention: headT_fthr[Letter, alphabetical].000[.L or .R or no suffix]. Center feather will have no suffix and start with 'A'. Feathers on the left or right will start with 'B' and have the corresponding suffix to indicate side", 
        default = False, 
    )
    prop_sc.fthr_hat_constraints = BoolProperty(  #Hat feather constraint option
        name = "Feather Hat Constraints", 
        description = "Toggle the use of automatic constraints for hat feathers. Must use the specific naming convention: hat_fthr[Letter, alphabetical].000", 
        default = False, 
    )
    prop_sc.fthr_wing_arms_constraints = BoolProperty(  #Wing-arm feather constraint option
        name = "Wing-arm Constraints", 
        description = "Toggle the use of automatic constraints for the wing-arm feathers. Must use the specific naming convention for metarig mch bones: MCH_fthr[Letter, alphabetical].000.[L or R]. Must use specific naming convention for control bones: fthr[Letter, alphabetical].000.[L or R]. Feather controls must be present in the metarig: fthrInner[_Curl].[L & R], fthrMid[_Curl].[L & R], and fthrOuter[_Curl].[L & R]", 
        default = False, 
    )
    prop_sc.fthr_wing_arms_innerAmount = IntProperty(   #Wind-arm feather, inside feather amount
        name = "Wing-Arm Inside Amount", 
        description = "Set the amount of feathers that make up the inside portion (closer to the body) of the wing. This will set how many feathers are controlled by the inner control. The feather just outside of this amount should be the middle feather, and all other feathers will be controlled by the outer controller", 
        default = 5, 
        min = 1, 
        max = 24, 
    )
    prop_sc.fthr_wing_arms_amount = IntProperty(
        name = "Wing-Arm Amount", 
        description = "Set the amount of feathers in a single wing-arm feather set. The amount should correspond to the amount on only one side, not both", 
        default = 16, 
        min = 1, 
        max = 26, 
    )
    prop_sc.fthr_wing_arms_influence = FloatVectorProperty(
        name = "Wing-Arm Middle Influence", 
        description = "Set the influence of the individual feathers in relation to the middle controller. Each value in this array will correspond to one feather (corresponding to the displayed letter) starting from the inside going outwards", 
        size = 26, 
        default = [0.0, 0.2, 0.4, 0.6, 0.8, 1.0, 1.0 , 0.875, 0.75, 0.625, 0.5, 0.4, 0.3, 0.2, 0.1, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0],
        min = 0.0, 
        max = 1.0, 
    )
    prop_sc.cwl_disable_foot_deform = BoolProperty(
        name = "Disable Foot Deform Bones", 
        description = "When toggled on, the toe and foot deform bones from the default Rigify leg will be disabled. Useful in Cowler rigs where other bones replace the use of these ones", 
        default = False, 
    )

    #Snaryan Deform
    prop_sc.snr_def_control = BoolProperty(
        name = "Deformation Control - Snaryan", 
        description = "Option for controlling which deformation-bone sets have their Deform option enabled upon rig-generation, Snaryan-specific. Useful for isolating certain bones for assigning automatic weights. If disabled, Deform options will not be modified from rig generation", 
        default = False, 
    )
    prop_sc.snr_def_teeth = BoolProperty(
        name = "DEF Teeth - Snaryan", 
        description = "Controls the Deform options for the teeth bones, Snaryan specific", 
        default = True, 
    )
    prop_sc.snr_def_tongue = BoolProperty(
        name = "DEF Tongue - Snaryan", 
        description = "Controls the Deform options for the tongue bones, Snaryan specific", 
        default = True, 
    )
    prop_sc.snr_def_eye = BoolProperty(
        name = "DEF Eye - Snaryan", 
        description = "Controls the Deform options for the eye bones, Snaryan specific", 
        default = True, 
    )
    prop_sc.snr_def_eyelid = BoolProperty(
        name = "DEF Eyelid - Snaryan", 
        description = "Controls the Deform options for the eyelid bones, Snaryan specific", 
        default = True, 
    )
    prop_sc.snr_def_backfin = BoolProperty(
        name = "DEF Backfin - Snaryan", 
        description = "Controls the Deform options for the backfin bones, Snaryan specific", 
        default = True, 
    )
    #General Deform
    prop_sc.def_hat = BoolProperty(
        name = "DEF Hat", 
        description = "Controls the Deform options for the hat bone. Applies to any character with the hat bone present", 
        default = True
    )
    #General Property settings
    prop_sc.props_standard = BoolProperty(  #Standard properties (including toon render settings)
        name = "Standard Properties", 
        description = "Option to have standard properties added to root bone upon rig-generation. Properties include standard overall color controls and toon-render settings", 
        default = True, 
    )
    prop_sc.props_eye = BoolProperty(
        name = "Eye Properties", 
        description = "Option to have standard properties added to eye control upon rig-generation. Properties include color and reflection controls specific for eyes", 
        default = True, 
    )
    prop_sc.props_colortype = EnumProperty(     #Various color options
        items = [
            ('NONE', "None", "No Color Properties", 1), 
            ('SNARYAN', "Snaryan", "Snaryan Color Properties", 2), 
            ('COWLER', "Cowler", "Cowler Color Properties", 3), 
        ], 
        name = "Color Properties", 
        description = "Option to control what type of color properties to add to the root when the rig is generated. Set to None to disable", 
        default = 1, 
    )
    #Color property settings & defaults
    prop_sc.props_basecolor = FloatVectorProperty(
        name = "Base Color", 
        description = "Set the default color to be used for the base color property when created", 
        size = 4, 
        default = (0.002400, 0.004512, 0.012000, 1.000000), 
        subtype = 'COLOR', 
        min = 0.0, 
        max = 10.0, 
    )
    prop_sc.props_primarycolor = FloatVectorProperty(
        name = "Primary Color", 
        description = "Set the default color to be used for the primary color property when created", 
        size = 4, 
        default = (0.009882, 0.009500, 0.010000, 1.000000), 
        subtype = 'COLOR', 
        min = 0.0, 
        max = 10.0, 
    )
    prop_sc.props_secondarycolor = FloatVectorProperty(
        name = "Secondary Color", 
        description = "Set the default color to be used for the secondary color property when created", 
        size = 4, 
        default = (0.035600, 0.050000, 0.005000, 1.000000), 
        subtype = 'COLOR', 
        min = 0.0, 
        max = 10.0, 
    )
    prop_sc.props_whitescolor = FloatVectorProperty(
        name = "Whites Color", 
        description = "Set the default color to be used for the whites color property when created", 
        size = 4, 
        default = (0.070000, 0.067200, 0.042000, 1.000000), 
        subtype = 'COLOR', 
        min = 0.0, 
        max = 10.0, 
    )
    prop_sc.props_darkscolor = FloatVectorProperty(
        name = "Darks Color", 
        description = "Set the default color to be used for the darks color property when created", 
        size = 4, 
        default = (0.001650, 0.002000, 0.001300, 1.000000), 
        subtype = 'COLOR', 
        min = 0.0, 
        max = 10.0, 
    )
    prop_sc.props_nailscolor = FloatVectorProperty(
        name = "Nails Color", 
        description = "Set the default color to be used for the nails color property when created", 
        size = 4, 
        default = (0.050000, 0.037000, 0.017500, 1.000000), 
        subtype = 'COLOR', 
        min = 0.0, 
        max = 10.0, 
    )
    prop_sc.props_mouthcolor = FloatVectorProperty(
        name = "Mouth Color", 
        description = "Set the default color to be used for the mouth color property when created", 
        size = 4, 
        default = (0.100000, 0.022690, 0.022690, 1.000000), 
        subtype = 'COLOR', 
        min = 0.0, 
        max = 10.0, 
    )
    prop_sc.props_teethcolor = FloatVectorProperty(
        name = "Teeth Color", 
        description = "Set the default color to be used for the teeth color property when created", 
        size = 4, 
        default = (0.800000, 0.654400, 0.520000, 1.000000), 
        subtype = 'COLOR', 
        min = 0.0, 
        max = 10.0, 
    )
    prop_sc.props_beakmaincolor = FloatVectorProperty(
        name = "Beak Main Color", 
        description = "Set the default color to be used for the beak's main color property when created", 
        size = 4, 
        default = (0.000976, 0.000900, 0.001000, 1.000000), 
        subtype = 'COLOR', 
        min = 0.0, 
        max = 10.0, 
    )
    prop_sc.props_beaksecondarycolor = FloatVectorProperty(
        name = "Beak Secondary Color", 
        description = "Set the default color to be used for the beak's secondary color property when created", 
        size = 4, 
        default = (0.024750, 0.024831, 0.025000, 1.000000), 
        subtype = 'COLOR', 
        min = 0.0, 
        max = 10.0, 
    )
    prop_sc.props_beaktertiarycolor = FloatVectorProperty(
        name = "Beak Tertiary Color", 
        description = "Set the default color to be used for the beak's tertiary color property when created", 
        size = 4, 
        default = (0.190000, 0.193260, 0.200000, 1.000000), 
        subtype = 'COLOR', 
        min = 0.0, 
        max = 10.0, 
    )
    prop_sc.props_tonguecolor1 = FloatVectorProperty(
        name = "Tongue Color 1", 
        description = "Set the default color to be used for the first tongue color property when created", 
        size = 4, 
        default = (0.270000, 0.013500, 0.013500, 1.000000), 
        subtype = 'COLOR', 
        min = 0.0, 
        max = 10.0, 
    )
    prop_sc.props_tonguecolor2 = FloatVectorProperty(
        name = "Tongue Color 2", 
        description = "Set the default color to be used for the second tongue color property when created", 
        size = 4, 
        default = (0.000270, 0.000014, 0.000014, 1.000000), 
        subtype = 'COLOR', 
        min = 0.0, 
        max = 10.0, 
    )


    ####################
    # Sub-Menu Toggles
    ####################
    #Boolean property, toon properties toggle
    prop_sc.toon_toggle = BoolProperty(
        name = "Toon Properties", 
        description = "Toggle the display of toon properties from the root bone", 
        default = True, 
    )
    #Boolean property, character main properties toggle
    prop_sc.main_toggle = BoolProperty(
        name = "Main Properties", 
        description = "Toggle the display of the character's main properties from the root bone", 
        default = True, 
    )
    #Boolean property, clothing properties toggle
    prop_sc.clothing_toggle = BoolProperty(
        name = "Clothing Properties", 
        description = "Toggle the display of the clothing properties from the root bone", 
        default = True, 
    )
    #Boolean property, other character properties toggle
    prop_sc.other_toggle = BoolProperty(
        name = "Other Properties", 
        description = "Toggle the display of the other character properties from the root bone", 
        default = True, 
    )
    #Boolean property, eye properties toggle
    prop_sc.eye_toggle = BoolProperty(
        name = "Eye Properties", 
        description = "Toggle the display of the eye properties from the eye-controller bone", 
        default = True, 
    )
    #Boolean property, selected bone properties
    prop_sc.sel_bone_toggle = BoolProperty(
        name = "Selected Bone Properties", 
        description = "Toggle the display of the selected bone properties if there are any", 
        default = True, 
    )

def clear_properties():
    props = [
        "var_cutoff",
    ]
    for p in props:
        if p in bpy.types.Scene.bl_rna.properties:
            exec("del bpy.types.Scene." + p)