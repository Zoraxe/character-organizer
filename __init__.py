# ##### BEGIN LICENSE BLOCK #####
#
# Copyright (C) <2023>  <ZoraxeMB>

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
# ##### END LICENSE BLOCK #####

bl_info = {
    "name" : "CharOrg",
    "author" : "ZoraxeMB",
    "description" : "Character Organization addon: Interfaces for accessing rig properties and operators for creating properties and driver controls",
    "blender" : (4, 2, 3),
    "version" : (1, 0, 0),
    "location" : "",
    "warning" : "",
    "category" : "Rigging"
}
import bpy
from bpy.props import *
from .corg_props import *#(filter_callback, init_properties, clear_properties)     #Functions to create scene properties
#Import all interface panels
from . import corg_panel
#Import all operators
from . import corg_charops

#Register and unregister all classes
ut = bpy.utils  #utils reference
def register():
    corg_charops.register()
    corg_panel.register()
    #register the properties
    init_properties()

def unregister():
    corg_charops.unregister()
    corg_panel.unregister()
    clear_properties()

if __name__ == "__main__":
    register()